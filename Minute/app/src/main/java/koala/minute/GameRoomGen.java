
package koala.minute;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.Random;

import tyrantgit.explosionfield.ExplosionField;

public class GameRoomGen extends AppCompatActivity {
    Firebase firebaseRef;
    GameRoom gameRoom;
    Game_Guess theGame;
    long me;
    long player2Status;
    String gameHolderIndicator;
    Boolean timerIssued = false;
    Boolean offline = false;
    int offlineGuessingNumber = 0;
    int offlineRangeLow = 1;
    int offlineRangeHigh = 0;

    private ExplosionField explosion;

    // Widgets
    TextView roomIDTextView;
    TextView player1TextView;
    TextView player2TextView;
    TextView player1_name;
    TextView player2_name;
    TextView message;
    TextView turnMessageTextView;
    TextView gameMessageTextView;
    TextView range;
    TextView timerTextView;
    TextView offlineMessage;
    TextView offlineRange;
    Button ready;
    Button start;
    Button guess;
    Button startOffline;
    Button offlineGuess;
    Button offlineRestart;
    EditText playerGuess;
    EditText inputMax;
    EditText offlinePlayerGuess;


    public class Game_Guess {
        private long guess1;
        private long guess2;
        private long guessingNumber;
        private long range_high;
        private long range_low;
        private long turn;
        private long winner;

        public Game_Guess() {
            // empty default constructor, necessary for Firebase to be able to deserialize
        }

        public Game_Guess(String roomid) {
            Random random = new Random();
            guessingNumber = 1+ random.nextInt(198);
            range_low = 1;
            range_high = 200;
            // winner is set to 0 to show that no one wins yet
            winner = 0;
            turn = 1;
        }
        public long getGuessingNumber() {return  guessingNumber;}
        public long getGuess1() {return guess1;}
        public long getGuess2() {return guess2;}
        public long getRange_low() {return range_low;}
        public long getRange_high() {return range_high;}
        public void setGuessingNumber(long guessingNumber) {this.guessingNumber = guessingNumber;}
        public void setGuess1(long guess1) {this.guess2 = guess1;}
        public void setGuess2(long guess2) {this.guess2 = guess2;}
        public void setRange_high(long range_high) {this.range_high = range_high;}
        public long getRangeHigh() {return range_high;}
        public long getRangeLow() {return range_low;}
        public void setRange_low(long range_low) {this.range_low = range_low;}
        public long getTurn() {return turn;}
        public void setTurn(long turn) {this.turn = turn;}
        public long getWinner() {return winner;}
        public void setWinner(long winner) {this.winner = winner;}
    }


    // Listener for GameRoom information
    ValueEventListener roomListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot snapshot) {
            //read from databse
            String player1 = snapshot.child("player1").getValue(String.class);
            String player2 = snapshot.child("player2").getValue(String.class);
            String player1ID = snapshot.child("player1ID").getValue(String.class);
            String player2ID = snapshot.child("player2ID").getValue(String.class);
            String gameHolderID = snapshot.child("gameHolderID").getValue(String.class);
            String roomId = "Room" + snapshot.child("roomID").getValue(String.class);
            long playerNum = snapshot.child("playerNum").getValue(long.class);
            boolean serverGameOn = snapshot.child("gameOn").getValue(boolean.class);
            boolean localGameOn = gameRoom.isGameOn();
            player2Status = snapshot.child("player2Status").getValue(long.class);


            if (player1 == null) {
                return;
            }

            //System.out.println("2status  " + player2Status);
            // Display notice messages for player1(host)
            if ((player2Status == 1) && (me == 1)) {
                String msg = "Wait for second player to ready";
                message.setText(msg);
                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_in_left);
                animation.setDuration(2500);
                message.startAnimation(animation);
                message.setVisibility(View.VISIBLE);
            } else if (player2Status == 2 && me == 1) {
                message.setVisibility(View.INVISIBLE);
                start.setVisibility(View.VISIBLE);
            }

            // Initial game starts - change visibility on the interface
            //System.out.println("localgameon:  " + localGameOn + " serverGameOn " + serverGameOn);
            if ((!localGameOn) && (serverGameOn)) {
                final Animation fadeOut = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_out);
                final Animation fadeIn = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_in);
                final String player1Final = player1;
                final String player2Final = player2;
                fadeOut.setDuration(500);
                fadeIn.setDuration(1000);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    initialInterfaceUpdateAnimation();
                } else {
                    start.startAnimation(fadeOut);
                    start.setVisibility(View.INVISIBLE);
                    roomIDTextView.startAnimation(fadeOut);
                    roomIDTextView.setVisibility(View.INVISIBLE);
                    player1TextView.startAnimation(fadeOut);
                    player1TextView.setVisibility(View.INVISIBLE);
                    player2TextView.startAnimation(fadeOut);
                    player2TextView.setVisibility(View.INVISIBLE);
                    player1_name.startAnimation(fadeOut);
                    player1_name.setVisibility(View.INVISIBLE);
                    player2_name.startAnimation(fadeOut);
                    player2_name.setVisibility(View.INVISIBLE);
                    message.startAnimation(fadeOut);
                    message.setVisibility(View.INVISIBLE);
                }

                new CountDownTimer(5000, 1000) {
                    public void onTick(long millisUntilFinished) {
                        timerIssued = false;
                        timerTextView.setVisibility(View.VISIBLE);
                        String msg = "Game starts in: " + millisUntilFinished / 1000 + "s";
                        timerTextView.setText(msg);
                    }

                    public void onFinish() {
                        timerIssued = true;
                        timerTextView.setVisibility(View.GONE);

                        // Update the interface
                        interfaceUpdate(player1Final, player2Final, 1, 0);

                        range.startAnimation(fadeIn);
                        range.setVisibility(View.VISIBLE);
                    }
                }.start();

            }

            if (!serverGameOn) {
                //TextView roomid_dis = (TextView) findViewById(R.id.roomid);
                roomIDTextView.setText(roomId);
                roomIDTextView.setVisibility(View.VISIBLE);
            }

            // Write into local gameRoom
            gameRoom.setPlayer1(player1);
            gameRoom.setPlayer2(player2);
            gameRoom.setPlayer1ID(player1ID);
            gameRoom.setPlayer2ID(player2ID);
            gameRoom.setGameHolderID(gameHolderID);
            gameRoom.setPlayerNum(playerNum);
            if (serverGameOn) {
                gameRoom.setGameOn();
            } else {
                gameRoom.setGameOff();
            }

            // Display players' name and show which player is holder
            if(gameHolderID.equals(player1ID)) {
                //String msg = player1 + gameHolderIndicator;
                String msg = player1;
                player1_name.setText(msg);
                player2_name.setText(player2);
            } else if (gameHolderID.equals(player2ID)) {
                player1_name.setText(player1);
                //String msg = player2 + gameHolderIndicator;
                String msg = player2;
                player2_name.setText(msg);
            }
        }
        @Override
        public void onCancelled(FirebaseError firebaseError) {
            System.err.println("roomListener failed: " + firebaseError.getMessage());
        }
    };


    // Listen for game_guess number information
    ValueEventListener gameListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot snapshot) {
            if(snapshot.getValue() == null) {return;}
            System.out.println("gameListener - snapshot: " + snapshot);
            long guessingNumber = snapshot.child("guessingNumber").getValue(Long.class);
            long guess1 = snapshot.child("guess1").getValue(long.class);
            long guess2 = snapshot.child("guess2").getValue(long.class);
            long range_high = snapshot.child("range_high").getValue(long.class);
            long range_low = snapshot.child("range_low").getValue(long.class);
            long winner = snapshot.child("winner").getValue(long.class);
            long turn = snapshot.child("turn").getValue(long.class);
            //System.out.println("gameListener - turn: " + turn);

            theGame.setGuessingNumber(guessingNumber);
            theGame.setGuess1(guess1);
            theGame.setGuess2(guess2);
            theGame.setRange_high(range_high);
            theGame.setRange_low(range_low);
            theGame.setWinner(winner);
            theGame.setTurn(turn);

            String player1 = gameRoom.getPlayer1();
            String player2 = gameRoom.getPlayer2();
            if (winner == 0 && timerIssued) {
                interfaceUpdate(player1, player2, theGame.getTurn(), 0);
            }

            // Someone wins
            if(winner != 0) {
                timerIssued = false;
                interfaceUpdate(player1, player2, theGame.getTurn(), winner);
                end(); // remove data under "Games"
                return;
            }
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {
            System.err.println("gameListener failed: " + firebaseError.getMessage());
        }
    };


    // Listen for play2 status
    ValueEventListener player2StatusListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            player2Status = dataSnapshot.child("player2Status").getValue(long.class);
            if (player2Status == 0 && me == 1) {
                String msg = "Please wait for second player to join the room...";
                message.setText(msg);
                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_in_left);
                animation.setDuration(1500);
                //animation.setRepeatCount(-1);
                message.startAnimation(animation);
                message.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {
            System.err.println("Player2 Status Listener failed: " + firebaseError.getMessage());
        }
    };


    // Update turnMessage and gameMessage
    private void interfaceUpdate(String player1, String player2, long turn, long winner) {
        Animation fadeIn = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_in);
        fadeIn.setDuration(1000);
        Animation fadeOut = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_out);
        fadeOut.setDuration(1000);

        System.out.println("interfaceUpdate");

        // game doesn't end, winner = 0
        if (winner == 0) {
            // It's my turn
            if (me == turn) {
                String msg = "Your turn!";
                turnMessageTextView.setText(msg);
                turnMessageTextView.startAnimation(fadeIn);
                turnMessageTextView.setVisibility(View.VISIBLE);
                msg = "Enter a number between";
                gameMessageTextView.setText(msg);
                gameMessageTextView.startAnimation(fadeIn);
                gameMessageTextView.setVisibility(View.VISIBLE);
                guess.startAnimation(fadeIn);
                guess.setVisibility(View.VISIBLE);
                playerGuess.startAnimation(fadeIn);
                playerGuess.setVisibility(View.VISIBLE);

                // Display range
                String range_display = Long.toString(theGame.getRangeLow()) + " - " + Long.toString(theGame.getRangeHigh());
                range.setText(range_display);
                range.startAnimation(fadeIn);
                range.setVisibility(View.VISIBLE);

                // Not my turn
            } else {
                gameMessageTextView.startAnimation(fadeOut);
                gameMessageTextView.setVisibility(View.INVISIBLE);
                guess.startAnimation(fadeOut);
                guess.setVisibility(View.INVISIBLE);
                playerGuess.startAnimation(fadeOut);
                playerGuess.setVisibility(View.INVISIBLE);

                String msg;
                if (me == 1) {
                    msg = player2 + "'s turn!";
                } else {
                    msg = player1 + "'s turn!";
                }
                turnMessageTextView.setText(msg);
                turnMessageTextView.startAnimation(fadeIn);
                turnMessageTextView.setVisibility(View.VISIBLE);

                // Display range
                String range_display = Long.toString(theGame.getRangeLow()) + " - " + Long.toString(theGame.getRangeHigh());
                range.setText(range_display);
                range.startAnimation(fadeIn);
                range.setVisibility(View.VISIBLE);
            }
        } else {
            String msg;
            if (winner == 1) {
                msg = gameRoom.getPlayer1() + " wins!";
            } else {
                msg = gameRoom.getPlayer2() + " wins!";
            }

            turnMessageTextView.setText(msg);
            turnMessageTextView.startAnimation(fadeIn);
            turnMessageTextView.setVisibility(View.VISIBLE);
            msg = "result: " + theGame.getGuessingNumber();
            gameMessageTextView.setText(msg);
            gameMessageTextView.startAnimation(fadeIn);
            gameMessageTextView.setVisibility(View.VISIBLE);
            range.startAnimation(fadeOut);
            range.setVisibility(View.INVISIBLE);

            //explosion.explode(range);
            //range.setVisibility(View.INVISIBLE);

            guess.startAnimation(fadeOut);
            guess.setVisibility(View.INVISIBLE);
            playerGuess.startAnimation(fadeOut);
            playerGuess.setVisibility(View.INVISIBLE);
        }
    }

    private void initialInterfaceUpdateAnimation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            final Button startButton = (Button) findViewById(R.id.start);
            int cx = startButton.getWidth() / 2;
            int cy = startButton.getHeight() / 2;
            float initialRadius = (float) Math.hypot(cx, cy);

            // create the animation (the final radius is zero)
            Animator anim =
                    ViewAnimationUtils.createCircularReveal(startButton, cx, cy, initialRadius, 0);
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    startButton.setVisibility(View.INVISIBLE);
                }
            });
            anim.start();

            final TextView roomIDTextView = (TextView) findViewById(R.id.roomid);
            cx = roomIDTextView.getWidth() / 2;
            cy = roomIDTextView.getHeight() / 2;
            initialRadius = (float) Math.hypot(cx, cy);

            // create the animation (the final radius is zero)
            anim =
                    ViewAnimationUtils.createCircularReveal(roomIDTextView, cx, cy, initialRadius, 0);
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    roomIDTextView.setVisibility(View.INVISIBLE);
                }
            });
            anim.start();

            final TextView player1TextView = (TextView) findViewById(R.id.player1);
            cx = player1TextView.getWidth() / 2;
            cy = player1TextView.getHeight() / 2;
            initialRadius = (float) Math.hypot(cx, cy);

            // create the animation (the final radius is zero)
            anim =
                    ViewAnimationUtils.createCircularReveal(player1TextView, cx, cy, initialRadius, 0);
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    player1TextView.setVisibility(View.GONE);
                }
            });
            anim.start();

            final TextView player2TextView = (TextView) findViewById(R.id.player2);
            cx = player2TextView.getWidth() / 2;
            cy = player2TextView.getHeight() / 2;
            initialRadius = (float) Math.hypot(cx, cy);

            // create the animation (the final radius is zero)
            anim =
                    ViewAnimationUtils.createCircularReveal(player2TextView, cx, cy, initialRadius, 0);
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    player2TextView.setVisibility(View.GONE);
                }
            });
            anim.start();

            final TextView player1NameTextView = (TextView) findViewById(R.id.player1Name);
            cx = player1NameTextView.getWidth() / 2;
            cy = player1NameTextView.getHeight() / 2;
            initialRadius = (float) Math.hypot(cx, cy);

            // create the animation (the final radius is zero)
            anim =
                    ViewAnimationUtils.createCircularReveal(player1NameTextView, cx, cy, initialRadius, 0);
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    player1NameTextView.setVisibility(View.GONE);
                }
            });
            anim.start();

            final TextView player2NameTextView = (TextView) findViewById(R.id.player2Name);
            cx = player2NameTextView.getWidth() / 2;
            cy = player2NameTextView.getHeight() / 2;
            initialRadius = (float) Math.hypot(cx, cy);

            // create the animation (the final radius is zero)
            anim =
                    ViewAnimationUtils.createCircularReveal(player2NameTextView, cx, cy, initialRadius, 0);
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    player2NameTextView.setVisibility(View.GONE);
                }
            });
            anim.start();

            final TextView messageTextView = (TextView) findViewById(R.id.message);
            cx = messageTextView.getWidth() / 2;
            cy = messageTextView.getHeight() / 2;
            initialRadius = (float) Math.hypot(cx, cy);

            // create the animation (the final radius is zero)
            anim =
                    ViewAnimationUtils.createCircularReveal(messageTextView, cx, cy, initialRadius, 0);
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    messageTextView.setVisibility(View.GONE);
                }
            });
            anim.start();
        }
    }


    // Player 2 - ready
    public void ready(View view) {
        Animation slideIn = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_in_left);
        slideIn.setDuration(1000);
        Animation slideOut = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_out_right);
        slideOut.setDuration(1000);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            final Button readyButton = (Button) findViewById(R.id.ready);
            int cx = readyButton.getWidth() / 2;
            int cy = readyButton.getHeight() / 2;
            float initialRadius = (float) Math.hypot(cx, cy);

            // create the animation (the final radius is zero)
            Animator anim =
                    ViewAnimationUtils.createCircularReveal(readyButton, cx, cy, initialRadius, 0);
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    readyButton.setVisibility(View.INVISIBLE);
                }
            });
            anim.start();
        } else {
            ready.startAnimation(slideOut);
            ready.setVisibility(View.INVISIBLE);
        }


        String msg = "Wait for " + gameRoom.getPlayer1() + " to start the game...";
        message.setText(msg);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int cx = message.getWidth() / 2;
            int cy = message.getHeight() / 2;
            float finalRadius = (float) Math.hypot(cx, cy);
            // create the animator for this view (the start radius is zero)
            Animator anim =
                    ViewAnimationUtils.createCircularReveal(message, cx, cy, 0, finalRadius);
            message.setVisibility(View.VISIBLE);
            anim.start();
        } else {
            message.startAnimation(slideIn);
            message.setVisibility(View.VISIBLE);
        }

        gameRoom.setPlayer2Status(2);
        firebaseRef.child("Rooms").child(gameRoom.getRoomID()).setValue(gameRoom);
        theGame = new Game_Guess(gameRoom.getRoomID());

        firebaseRef.child("Games").child(gameRoom.getRoomID()).addValueEventListener(gameListener);
    }


    // Player1 -- Start
    public void startGame(View view) {
        Animation slideOut = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_out_right);
        slideOut.setDuration(1000);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            final Button startButton = (Button) findViewById(R.id.start);
            int cx = startButton.getWidth() / 2;
            int cy = startButton.getHeight() / 2;
            float initialRadius = (float) Math.hypot(cx, cy);

            // create the animation (the final radius is zero)
            Animator anim =
                    ViewAnimationUtils.createCircularReveal(startButton, cx, cy, initialRadius, 0);
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    startButton.setVisibility(View.INVISIBLE);
                }
            });
            anim.start();
        } else {
            start.startAnimation(slideOut);
            start.setVisibility(View.INVISIBLE);
        }

        //String restart = start.getText().toString();
        //if(restart.equals("restart")) {firebaseRef.child("Games").child(gameRoom.getRoomID()).removeEventListener(gameListener);}

        firebaseRef.child("Rooms").child(gameRoom.getRoomID()).child("gameOn").setValue(true);
        theGame = new Game_Guess(gameRoom.getRoomID());
        firebaseRef.child("Games").child(gameRoom.getRoomID()).setValue(theGame);
        firebaseRef.child("Games").child(gameRoom.getRoomID()).addValueEventListener(gameListener);
    }


    public void guess(View view) throws IOException {
        if(theGame.getTurn() != me) {return;}
        EditText guessingText = (EditText) findViewById(R.id.playerGuess);
        String guessString = guessingText.getText().toString();
        guessingText.setText("");
        long lowLong = theGame.getRange_low();
        long highLong = theGame.getRange_high();
        int low = (int) lowLong;
        int high = (int) highLong;

        int guessInt;
        try {
            guessInt = Integer.parseInt(guessString);
            if ((guessInt <= low) || (guessInt >= high)) {
                Toast.makeText(getApplicationContext(), "Out of Range", Toast.LENGTH_LONG).show();
            } else {
                if(guessInt == theGame.getGuessingNumber()) {
                    theGame.setWinner(theGame.getTurn());
                }
                if (guessInt < theGame.getGuessingNumber()) {
                    Toast.makeText(getApplicationContext(), "Should be higher", Toast.LENGTH_LONG).show();
                    theGame.setRange_low(guessInt);
                } else if (guessInt > theGame.getGuessingNumber()){
                    Toast.makeText(getApplicationContext(), "Should be lower", Toast.LENGTH_LONG).show();
                    theGame.setRange_high(guessInt);
                }

                if(theGame.getTurn() == 1) {
                    theGame.setGuess1(guessInt);
                    theGame.setTurn(2);
                }
                else if (theGame.getTurn() == 2) {
                    theGame.setGuess2(guessInt);
                    theGame.setTurn(1);
                }
                else {System.err.println("Game turn is not 1 or 2, ERROR");}

                //upload
                firebaseRef.child("Games").child(gameRoom.getRoomID()).setValue(theGame);
            }


        } catch (NumberFormatException e) {
            Toast.makeText(getApplicationContext(), "Please enter a number", Toast.LENGTH_LONG).show();
        }
    }

    // Remove data under "Games"
    private void end() {
        gameRoom.setGameOff();
        firebaseRef.child("Rooms").child(gameRoom.getRoomID()).child("gameOn").setValue(false);
        firebaseRef.child("Games").child(gameRoom.getRoomID()).setValue(null);
    }

    // When player leaves the room
    public void leave(View view) {
        firebaseRef.child("Games").child(gameRoom.getRoomID()).removeEventListener(gameListener);
        firebaseRef.child("Games").child(gameRoom.getRoomID()).removeEventListener(roomListener);
        gameRoom.decrementPlayerNum();
        //System.out.println("num: " + gameRoom.getPlayerNum());

        if (gameRoom.getPlayerNum() == 1) {
            firebaseRef.child("Rooms").child(gameRoom.getRoomID()).setValue(gameRoom);
        } else {
            firebaseRef.child("Rooms").child(gameRoom.getRoomID()).setValue(null);
            if (gameRoom.isGameOn()) {
                firebaseRef.child("Games").child(gameRoom.getRoomID()).setValue(null);
            }
        }

        finish();
    }


    public void startOffline(View view) {
        RelativeLayout offlineLayout = (RelativeLayout) findViewById(R.id.offlineLayout);

        if (String.valueOf(inputMax.getText()).equals("")) {
            Toast.makeText(getApplicationContext(), "Please input a number", Toast.LENGTH_LONG).show();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                final RelativeLayout inputLayoutFinal = (RelativeLayout) findViewById(R.id.inputLayout);
                int cx = inputLayoutFinal.getWidth() / 2;
                int cy = inputLayoutFinal.getHeight() / 2;
                float initialRadius = (float) Math.hypot(cx, cy);

                // create the animation (the final radius is zero)
                Animator anim =
                        ViewAnimationUtils.createCircularReveal(inputLayoutFinal, cx, cy, initialRadius, 0);
                anim.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        inputLayoutFinal.setVisibility(View.INVISIBLE);
                    }
                });
                anim.start();


                cx = offlineLayout.getWidth() / 2;
                cy = offlineLayout.getHeight() / 2;
                float finalRadius = (float) Math.hypot(cx, cy);
                anim =
                        ViewAnimationUtils.createCircularReveal(offlineLayout, cx, cy, 0, finalRadius);
                offlineLayout.setVisibility(View.VISIBLE);
                anim.start();
            } else {
                RelativeLayout inputLayout = (RelativeLayout) findViewById(R.id.inputLayout);
                inputLayout.setVisibility(View.GONE);
                offlineLayout.setVisibility(View.VISIBLE);
            }

            offlineRange.setVisibility(View.VISIBLE);
            offlinePlayerGuess.setVisibility(View.VISIBLE);
            offlineGuess.setVisibility(View.VISIBLE);
            offlineRestart.setVisibility(View.INVISIBLE);

            offlineRangeLow = 1;
            int max = Integer.parseInt(String.valueOf(inputMax.getText()));
            inputMax.setText("");
            offlineRangeHigh = max;
            Random random = new Random();
            offlineGuessingNumber = 2 + random.nextInt(max-2);

            String msg = "1 - " + max;
            offlineRange.setText(msg);
        }
    }


    public void offlineGuess(View view) {
        Animation fadeIn = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_in);
        fadeIn.setDuration(1000);
        Animation fadeOut = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_out);
        fadeOut.setDuration(1000);

        String guess = offlinePlayerGuess.getText().toString();
        System.out.println("offline guessing: " + offlineGuessingNumber + " low: " + offlineRangeLow + " high: " + offlineRangeHigh);

        offlinePlayerGuess.setText("");
        if (guess.equals("")) {
            Toast.makeText(getApplicationContext(), "Please input a number", Toast.LENGTH_LONG).show();
        } else {
            int guessInt = Integer.parseInt(guess);
            if (guessInt < offlineRangeLow || guessInt > offlineRangeHigh-1) {
                Toast.makeText(getApplicationContext(), "Out of range", Toast.LENGTH_LONG).show();
            } else {
                // Win
                if (guessInt == offlineGuessingNumber) {
                    String msg = "Bingo! Result is: " + offlineGuessingNumber;
                    offlineMessage.setText(msg);

                    offlineRange.startAnimation(fadeOut);
                    offlineRange.setVisibility(View.INVISIBLE);
                    offlinePlayerGuess.startAnimation(fadeOut);
                    offlinePlayerGuess.setVisibility(View.INVISIBLE);
                    offlineGuess.startAnimation(fadeOut);
                    offlineGuess.setVisibility(View.INVISIBLE);
                    offlineRestart.startAnimation(fadeIn);
                    offlineRestart.setVisibility(View.VISIBLE);

                } else if (guessInt < offlineGuessingNumber) {
                    Toast.makeText(getApplicationContext(), "Should be higher", Toast.LENGTH_LONG).show();
                    offlineRangeLow = guessInt;
                    String range = offlineRangeLow + " - " + offlineRangeHigh;
                    offlineRange.setText(range);
                    offlineRange.startAnimation(fadeIn);
                } else {
                    Toast.makeText(getApplicationContext(), "Should be lower", Toast.LENGTH_LONG).show();
                    offlineRangeHigh = guessInt;
                    String range = offlineRangeLow + " - " + offlineRangeHigh;
                    offlineRange.setText(range);
                    offlineRange.startAnimation(fadeIn);
                }
            }
        }
    }


    public void offlineRestart(View view) {
        RelativeLayout offlineLayout = (RelativeLayout) findViewById(R.id.offlineLayout);
        RelativeLayout inputLayout = (RelativeLayout) findViewById(R.id.inputLayout);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            final RelativeLayout offlineLayoutFinal = (RelativeLayout) findViewById(R.id.offlineLayout);
            int cx = offlineLayoutFinal.getWidth() / 2;
            int cy = offlineLayoutFinal.getHeight() / 2;
            float initialRadius = (float) Math.hypot(cx, cy);

            // create the animation (the final radius is zero)
            Animator anim =
                    ViewAnimationUtils.createCircularReveal(offlineLayoutFinal, cx, cy, initialRadius, 0);
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    offlineLayoutFinal.setVisibility(View.INVISIBLE);
                }
            });
            anim.start();


            cx = inputLayout.getWidth() / 2;
            cy = inputLayout.getHeight() / 2;
            float finalRadius = (float) Math.hypot(cx, cy);
            anim =
                    ViewAnimationUtils.createCircularReveal(inputLayout, cx, cy, 0, finalRadius);
            inputLayout.setVisibility(View.VISIBLE);
            anim.start();
        } else {
            offlineLayout.setVisibility(View.GONE);
            inputLayout.setVisibility(View.VISIBLE);
        }
    }

    public void offlineLeave(View view) {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_room_gen);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        // Initialization
        firebaseRef = new Firebase("https://koalaminute.firebaseio.com/");
        String UID = "1111";
        AuthData authData = firebaseRef.getAuth();
        if (authData != null) {
            UID = authData.getUid();
            System.out.println("UID: " + UID);
        } else {
            System.err.println("No authData in GameRoomGen");
        }

        //setup
        if(UID.equals("1111")) {System.err.println("Something wrong in GameRoomGen creation auth");}
        final String waitingPlayerID = "0";
        player2Status = 0;
        gameHolderIndicator = "(GameHolder)";

        // Widgets
        roomIDTextView = (TextView) findViewById(R.id.roomid);
        player1TextView = (TextView) findViewById(R.id.player1);
        player2TextView = (TextView) findViewById(R.id.player2);
        player1_name = (TextView) findViewById(R.id.player1Name);
        player2_name = (TextView) findViewById(R.id.player2Name);
        message = (TextView)findViewById(R.id.message);
        turnMessageTextView = (TextView)findViewById(R.id.turnMessage);
        gameMessageTextView = (TextView)findViewById(R.id.gameMessage);
        range = (TextView) findViewById(R.id.rangeDisplay);
        timerTextView = (TextView) findViewById(R.id.timer);
        offlineMessage = (TextView) findViewById(R.id.offlineMessage);
        offlineRange = (TextView) findViewById(R.id.offlineRange);
        start = (Button) findViewById(R.id.start);
        ready = (Button) findViewById(R.id.ready);
        guess = (Button) findViewById(R.id.guess);
        startOffline = (Button) findViewById(R.id.startOffline);
        offlineGuess = (Button) findViewById(R.id.offlineGuess);
        offlineRestart = (Button) findViewById(R.id.offlineRestart);
        playerGuess = (EditText) findViewById(R.id.playerGuess);
        inputMax = (EditText) findViewById(R.id.inputMax);
        offlinePlayerGuess = (EditText) findViewById(R.id.offlinePlayerGuess);

        // Explosion animation
        explosion = ExplosionField.attach2Window(this);


        final Bundle roomid = getIntent().getExtras();
        String id = "";
        System.out.println(firebaseRef.child("RoomIDcounter"));

        // If the player is creating a new room
        if (roomid == null) {
            me = 1;
            System.out.println("new room");
            gameRoom = new GameRoom(UID);

            ValueEventListener createRoom = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    // Retrieve RoomIDcounter from server and increment it by 1
                    String currentRoomID = snapshot.getValue(String.class);
                    String newRoomID = Integer.toString(Integer.parseInt(currentRoomID) + 1);
                    // Save the updated RoomIDcounter on server
                    firebaseRef.child("RoomIDcounter").setValue(newRoomID);
                    System.out.println("set " + newRoomID);
                    gameRoom.setRoomID(newRoomID);
                    gameRoom.incrementPlayerNum();


                    //String roomID = gameRoom.getRoomID();
                    //System.out.println(roomID);
                    firebaseRef.child("Rooms").child(newRoomID).setValue(gameRoom);
                    firebaseRef.child("Rooms").child(gameRoom.getRoomID()).addValueEventListener(roomListener);
                    firebaseRef.child("Rooms").child(gameRoom.getRoomID()).addListenerForSingleValueEvent(player2StatusListener);
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {
                    System.err.println("createRoom listener failed -- host");
                }
            };
            firebaseRef.child("RoomIDcounter").addListenerForSingleValueEvent(createRoom);
            firebaseRef.removeEventListener(createRoom);


        } else {    // If the player is joining the game
            Button ready = (Button) findViewById(R.id.ready);
            ready.setVisibility(View.VISIBLE);
            id = roomid.getString("ID");

            if (id.equals("offline")) {
                offline = true;
                RelativeLayout onlineLayout = (RelativeLayout) findViewById(R.id.onlineLayout);
                //RelativeLayout offlineLayout = (RelativeLayout) findViewById(R.id.offlineLayout);
                RelativeLayout inputLayout = (RelativeLayout) findViewById(R.id.inputLayout);
                onlineLayout.setVisibility(View.GONE);
                //offlineLayout.setVisibility(View.GONE);
                inputLayout.setVisibility(View.VISIBLE);

            } else {
                //System.out.println("join new room " + id);
                gameRoom = new GameRoom();
                ValueEventListener createRoom = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        if (snapshot == null) {
                            Toast.makeText(getApplicationContext(), "Invalid Game Room ID.", Toast.LENGTH_LONG).show();
                            finish();
                        }
                        System.out.println(snapshot);
                        //getting the data from database
                        String player1 = snapshot.child("player1").getValue(String.class);
                        String player2 = snapshot.child("player2").getValue(String.class);
                        String player1ID = snapshot.child("player1ID").getValue(String.class);
                        String player2ID = snapshot.child("player2ID").getValue(String.class);
                        String RoomID = snapshot.child("roomID").getValue(String.class);
                        long playerNumServer = snapshot.child("playerNum").getValue(long.class);

                        //storing the data
                        gameRoom.setGameHolderID(snapshot.child("gameHolderID").getValue(String.class));
                        gameRoom.setRoomID(RoomID);
                        gameRoom.setPlayer1(player1);
                        gameRoom.setPlayer2(player2);
                        gameRoom.setPlayer1ID(player1ID);
                        gameRoom.setPlayer2ID(player2ID);
                        gameRoom.setPlayerNum(playerNumServer);
                        gameRoom.incrementPlayerNum();
                        gameRoom.setPlayer2Status(1);
                        firebaseRef.child("Rooms").child(gameRoom.getRoomID()).setValue(gameRoom);
                        firebaseRef.child("Rooms").child(gameRoom.getRoomID()).addValueEventListener(roomListener);
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {
                        System.err.println("createRoom listener failed -- other players");
                    }
                };
                firebaseRef.child("Rooms").child(id).addListenerForSingleValueEvent(createRoom);
                firebaseRef.removeEventListener(createRoom);
            }


        }



        if (!offline) {
            String player1_name_string = "";
            String player2_name_string = "waiting...";
            final String myID = UID;
            final String roomID = gameRoom.getRoomID();
            System.out.println("room is " + roomID);

            ValueEventListener updateName = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    System.out.println(snapshot);
                    //User thisUser = snapshot.getValue(User.class);

                    if(roomid == null) {
                        String roomID = gameRoom.getRoomID();
                        String player1 = snapshot.child("displayName").getValue(String.class);
                        gameRoom.setPlayer1(player1); //player1ID is already set
                        System.out.println("room is " + roomID);
                        firebaseRef.child("Rooms").child(roomID).child("player1").setValue(player1);
                        //player1_name.setText(player1 + "(GameHolder)");
                        player1_name.setText(player1);
                    } else {
                        String roomID = gameRoom.getRoomID();
                        String joinPlayer = snapshot.child("displayName").getValue(String.class);
                        if(gameRoom.getPlayer1ID() == waitingPlayerID) {
                            me = 1;

                            gameRoom.setPlayer1(joinPlayer);
                            gameRoom.setPlayer1ID(myID);
                            firebaseRef.child("Rooms").child(roomID).child("player1").setValue(joinPlayer);
                            firebaseRef.child("Rooms").child(roomID).child("player1ID").setValue(myID);
                            player1_name.setText(joinPlayer);
                            //player2_name.setText(gameRoom.getPlayer1()+ gameHolderIndicator);
                            player2_name.setText(gameRoom.getPlayer1());
                        }

                        else {
                            me =2;

                            gameRoom.setPlayer2(joinPlayer);
                            gameRoom.setPlayer2ID(myID);
                            firebaseRef.child("Rooms").child(roomID).child("player2").setValue(joinPlayer);
                            firebaseRef.child("Rooms").child(roomID).child("player2ID").setValue(myID);
                            player2_name.setText(joinPlayer);
                            //player1_name.setText(gameRoom.getPlayer1() + gameHolderIndicator);
                            player1_name.setText(gameRoom.getPlayer1());
                        }
                    }
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {
                    System.out.println("User/UserID create failed");
                }
            };

            firebaseRef.child("Users").child(UID).addListenerForSingleValueEvent(updateName);
            firebaseRef.removeEventListener(updateName);
        }

    }

}

