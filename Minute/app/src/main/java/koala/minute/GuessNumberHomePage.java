package koala.minute;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.transition.Explode;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

public class GuessNumberHomePage extends AppCompatActivity {
    private Animation animationtrans;
    Firebase firebaseRef;
    TextView description;
    String UID;
    SharedPreferences sharedPreferences;

    public void Guess_Number(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setExitTransition(new Explode());
            Intent intent = new Intent(this, GameRoomGen.class);
            startActivity(intent,
                    ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else {
            view.startAnimation(animationtrans);
            Intent intent = new Intent(this, GameRoomGen.class);
            startActivity(intent);
        }
    }

    public void createOfflineGuess(View view) {
        Intent intent = new Intent(this, GameRoomGen.class);
        Bundle RoomID = new Bundle();
        RoomID.putString("ID", "offline");
        intent.putExtras(RoomID);
        startAnimation(intent);
    }


    public void join(View view){
        joinGuess();
    }

    private void joinGuess() {
        //view.startAnimation(animationtrans);
        EditText room_id = (EditText) findViewById(R.id.inputMax);
        final String parseId = room_id.getText().toString();
        room_id.setText("");
        if (parseId.equals("")) {
            Toast.makeText(getApplicationContext(), "Invalid Game Room ID.", Toast.LENGTH_LONG).show();
        } else {
            final Intent intent = new Intent(this, GameRoomGen.class);

            // Pass the roomID to new activity
            ValueEventListener joinRoom = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    if (snapshot.getValue() == null) {
                        Toast.makeText(getApplicationContext(), "Invalid Game Room ID.", Toast.LENGTH_LONG).show();
                        return;
                    }
                    Bundle RoomID = new Bundle();
                    RoomID.putString("ID", parseId);
                    intent.putExtras(RoomID);

                    startAnimation(intent);
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {
                    System.out.println("join room cancelld");
                }
            };
            firebaseRef.child("Rooms").child(parseId).addListenerForSingleValueEvent(joinRoom);
            firebaseRef.removeEventListener(joinRoom);
        }
    }


    private void startAnimation(Intent intent){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setExitTransition(new Explode());
            startActivity(intent,
                    ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        }
    }

    public void swapIntoJoinLayout(View view) {
        final RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);
        final RelativeLayout joinLayout = (RelativeLayout) findViewById(R.id.joinLayout);

        Animation slideIn = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_in_left);
        Animation slideOut = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_out_right);
        slideIn.setDuration(300);
        slideOut.setDuration(300);

        mainLayout.startAnimation(slideOut);
        mainLayout.setVisibility(View.GONE);
        joinLayout.startAnimation(slideIn);
        joinLayout.setVisibility(View.VISIBLE);
    }


    public void swapIntoMainLayout(View view) {
        final RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);
        final RelativeLayout joinLayout = (RelativeLayout) findViewById(R.id.joinLayout);

        Animation slideIn = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_in_left);
        Animation slideOut = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_out_right);
        slideIn.setDuration(300);
        slideOut.setDuration(300);

        joinLayout.startAnimation(slideOut);
        joinLayout.setVisibility(View.GONE);
        mainLayout.startAnimation(slideIn);
        mainLayout.setVisibility(View.VISIBLE);
    }


    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        final RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);

        switch (keyCode) {
            case KeyEvent.KEYCODE_ENTER:
                joinGuess();
                return true;
            case KeyEvent.KEYCODE_BACK:
                if (mainLayout.getVisibility() == View.GONE) {
                    swapIntoMainLayout(this.getWindow().getDecorView());
                } else {
                    finish();
                }
                return true;
            default:
                return true;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guess_number_home_page);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        animationtrans = new AnimationUtils().loadAnimation(this,R.anim.anim_translate);

        firebaseRef = new Firebase("https://koalaminute.firebaseio.com/");
        description = (TextView) findViewById(R.id.descriptionGuess);
        description.setMovementMethod(new ScrollingMovementMethod());
        sharedPreferences = this.getSharedPreferences("koala.minute", Context.MODE_PRIVATE);

        AuthData authData = firebaseRef.getAuth();
        if (authData != null) {
            UID = authData.getUid();
            System.out.println("UID: " + UID);
        } else {
            System.out.println("No authData in HomePage");
        }
        if(UID == null) {
            System.out.println("home page no auth, wh");
            firebaseRef.unauth();
            Intent intent = new Intent(this, LoginSignup.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return;
        }
    }

}
