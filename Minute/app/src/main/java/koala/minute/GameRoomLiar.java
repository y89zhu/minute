package koala.minute;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.core.SnapshotHolder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

//the map of userinfo, the value is an integer(indicate playernumber) followed by the displayname
public class GameRoomLiar {
    private String gameHolderID;
    private String roomID;
    private String type;
    private String playerNum;
    private Map<String,String> userInfo;
    private String readyNum;

    private boolean gameOn;
    private String leaveNum;

    public GameRoomLiar() {}

    public GameRoomLiar(String gameHolderID, String gameHolderName) {
        readyNum = "1";
        this.gameHolderID = gameHolderID;
        this.type = "liar";
        this.playerNum = "1";
        userInfo = new HashMap<String,String>();
        userInfo.put(gameHolderID,"1"+gameHolderName);
        leaveNum = "0";
        gameOn = false;
    }
    public String getReadyNum() {return  readyNum;}
    public void setReadyNum(String readyNum) {this.readyNum = readyNum;}

    public String getRoomID() {return roomID;}
    public void setRoomID(String roomID) {this.roomID = roomID;}

    public String getType() {return type;}
    public void setType(String type) {this.type = type;}

    public String getPlayerNum() {return playerNum;}
    public void setPlayerNum(String playerNum) {this.playerNum = playerNum;}

    public boolean isGameOn() {
        return  gameOn;
    }
    public void setGameOn(boolean gameOn) {
        this.gameOn = gameOn;
    }

    public String getLeaveNum() {return  leaveNum;}
    public String getGameHolderID() {return  gameHolderID;}

    public void addPlayer(String id, String name) {
        int temp = Integer.parseInt(playerNum);
        temp++;
        playerNum = Integer.toString(temp);
        userInfo.put(id,playerNum+name);
    }
    public void removePlayer(String id) {
        System.out.println("room remove player " + userInfo);
        int temp = Integer.parseInt(playerNum);
        playerNum = Integer.toString(temp - 1);
        //get deleted player's turn
        int deleteTurn = Integer.parseInt(userInfo.get(id).substring(0,1));

        //update each player's turn
        userInfo.remove(id);
        leaveNum = Integer.toString(deleteTurn);
        for (Map.Entry<String, String> entry : userInfo.entrySet()) {
            String tempName = entry.getValue();
            int tempTurn = Integer.parseInt(tempName.substring(0,1));
            if (deleteTurn < tempTurn) {
                tempTurn--;
                entry.setValue(tempTurn+tempName.substring(1));
            }

        }
        Firebase roomRef = new Firebase("https://koalaminute.firebaseio.com/Rooms/"+roomID+"/");

        for (Map.Entry<String, String> entry : userInfo.entrySet()) {
            roomRef.child("userInfo").child(entry.getKey()).setValue(entry.getValue());
        }
        roomRef.child("userInfo").child(id).setValue(null);

    }
    public Map<String, String>getUserInfo() {
        return userInfo;
    }



    //add this two methods for convenience
    public void update(DataSnapshot snapshot) {
        gameHolderID = snapshot.child("gameHolderID").getValue(String.class);
        roomID = snapshot.child("roomID").getValue(String.class);
        type = snapshot.child("type").getValue(String.class);

        gameOn = snapshot.child("gameOn").getValue(boolean.class);
        readyNum = snapshot.child("readyNum").getValue(String.class);
        leaveNum = snapshot.child("leaveNum").getValue(String.class);
        playerNum = snapshot.child("playerNum").getValue(String.class);
        userInfo = new HashMap<String,String>();

        for (DataSnapshot userSnapshot: snapshot.child("userInfo").getChildren()) {
            String check = userInfo.put(userSnapshot.getKey().toString(), userSnapshot.getValue(String.class));
            if (check == null) {}
        }



    }

    public void upload() {
        Firebase roomRef = new Firebase("https://koalaminute.firebaseio.com/Rooms/"+roomID+"/");
        //roomRef.setValue(this);
        roomRef.child("roomID").setValue(roomID);
        roomRef.child("gameHolderID").setValue(gameHolderID);
        roomRef.child("type").setValue(type);
        roomRef.child("readyNum").setValue(readyNum);
        roomRef.child("leaveNum").setValue(leaveNum);
        roomRef.child("playerNum").setValue(playerNum);
        roomRef.child("gameOn").setValue(gameOn);

        for (Map.Entry<String, String> entry : userInfo.entrySet()) {
            roomRef.child("userInfo").child(entry.getKey()).setValue(entry.getValue());
        }

    }

}
