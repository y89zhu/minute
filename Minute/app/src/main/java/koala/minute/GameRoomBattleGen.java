package koala.minute;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class GameRoomBattleGen extends AppCompatActivity {

    Firebase firebaseRef;
    String UID;
    String displayName;
    int me;

    // Game-related constants
    GameRoomBattle gameRoomBattle;
    GameBattle gameBattle;
    long originalTroop = 100;
    long baseDecrement = 20;
    Boolean newTurnAvailable = false;

    // Widgets
    TextView roomNumber;
    TextView player1Name;
    TextView player2Name;
    TextView player3Name;
    TextView player4Name;
    TextView message;
    TextView gameMessage;
    TextView resultMessage;
    Button start;
    Button ready;
    Button attackOrDefend;
    RadioGroup radioGroup_attackOrDefend;
    RadioGroup radioGroup_choosePlayer;
    RadioButton radio_player1;
    RadioButton radio_player2;
    RadioButton radio_player3;



    public class GameBattle {
        private String roomID;
        private long winner;
        private long turn;
        private String trait1;
        private String trait2;
        private String trait3;
        private String trait4;
        private String class1;
        private String class2;
        private String class3;
        private String class4;
        private long player1Troop;
        private long player2Troop;
        private long player3Troop;
        private long player4Troop;
        private String player1action;
        private String player2action;
        private String player3action;
        private String player4action;
        private String player1Result;
        private String player2Result;
        private String player3Result;
        private String player4Result;
        private boolean resultAvailable;

        public GameBattle() {}

        public GameBattle(String roomID) {
            this.roomID = roomID;
            this.winner = 0;
            this.turn = 1;
            this.trait1 = "";
            this.trait2 = "";
            this.trait3 = "";
            this.trait4 = "";
            this.class1 = "";
            this.class2 = "";
            this.class3 = "";
            this.class4 = "";
            this.player1Troop = originalTroop;
            this.player2Troop = originalTroop;
            this.player3Troop = originalTroop;
            this.player4Troop = originalTroop;
            this.player1action = "";
            this.player2action = "";
            this.player3action = "";
            this.player4action = "";
            this.player1Result = "";
            this.player2Result = "";
            this.player3Result = "";
            this.player4Result = "";
            this.resultAvailable = false;
        }

        public String getRoomID() {return roomID;}
        public long getWinner() {return winner;}
        public long getTurn() {return turn;}
        public void setTurn(long turn) {this.turn = turn;}
        public void incrementTurn() {this.turn++;}
        public long getPlayer1Troop() {return player1Troop;}
        public long getPlayer2Troop() {return player2Troop;}
        public long getPlayer3Troop() {return player3Troop;}
        public long getPlayer4Troop() {return player4Troop;}
        public String getPlayer1action() {return player1action;}
        public String getPlayer2action() {return player2action;}
        public String getPlayer3action() {return player3action;}
        public String getPlayer4action() {return player4action;}
        public String getPlayer1Result() {return player1Result;}
        public String getPlayer2Result() {return player2Result;}
        public String getPlayer3Result() {return player3Result;}
        public String getPlayer4Result() {return player4Result;}
        public boolean isResultAvailable() {return resultAvailable;}
        public void setResultAvailableTrue() {resultAvailable = true;}
        public void setResultAvailableFalse() {resultAvailable = false;}


        public String gameEnd() {
            ArrayList<Integer> arrayList = new ArrayList<>();
            if (player1Troop <= 0 || player2Troop <= 0 || player3Troop <= 0 || player4Troop <= 0) {
                arrayList.add((int)(long)player1Troop);
                if (gameRoomBattle.getPlayer2Status() == 2) {
                    arrayList.add((int)(long)player2Troop);
                }
                if (gameRoomBattle.getPlayer3Status() == 2) {
                    arrayList.add((int)(long)player3Troop);
                }
                if (gameRoomBattle.getPlayer4Status() == 2) {
                    arrayList.add((int)(long)player4Troop);
                }
                int max = Collections.max(arrayList);
        System.out.println("max: " + max);

                if (max == player1Troop) {
                    return gameRoomBattle.getPlayer1();
                } else if (max == player2Troop) {
                    return gameRoomBattle.getPlayer2();
                } else if (max == player3Troop) {
                    return gameRoomBattle.getPlayer3();
                } else if (max == player4Troop) {
                    return gameRoomBattle.getPlayer4();
                }
            }
            return "";
        }

        public void resetTroop() {
            this.player1Troop = originalTroop;
            this.player2Troop = originalTroop;
            this.player3Troop = originalTroop;
            this.player4Troop = originalTroop;
        }

        public void appendAction(int player, String action) {
            if (player == 1) {
                player1action = player1action + action;
            } else if (player == 2) {
                player2action = player2action + action;
            } else if (player == 3) {
                player3action = player3action + action;
            } else if (player == 4) {
                player4action = player4action + action;
            }
        }

        public void appendResult(int player, String result) {
            if (player == 1) {
                player1Result = player1Result + result + "\n";
            } else if (player == 2) {
                player2Result = player2Result + result + "\n";
            } else if (player == 3) {
                player3Result = player3Result + result + "\n";
            } else if (player == 4) {
                player4Result = player4Result + result + "\n";
            }
        }

        public void uploadAction(int player) {
            Firebase roomRef = new Firebase("https://koalaminute.firebaseio.com/Games/"+roomID+"/");
            if (player == 1) {
                roomRef.child("player1action").setValue(player1action);
            } else if (player == 2) {
                roomRef.child("player2action").setValue(player2action);
            } else if (player == 3) {
                roomRef.child("player3action").setValue(player3action);
            } else if (player == 4) {
                roomRef.child("player4action").setValue(player4action);
            }
        }

        public void removeAllAction() {
            player1action = "";
            player2action = "";
            player3action = "";
            player4action = "";
        }

        public void removeAllResult() {
            player1Result = "";
            player2Result = "";
            player3Result = "";
            player4Result = "";
        }

        public void decrementTroop(int player, long amount) {
            if (player == 1) {
                player1Troop = player1Troop - amount;
            } else if (player == 2) {
                player2Troop = player2Troop - amount;
            } else if (player == 3) {
                player3Troop = player3Troop - amount;
            } else if (player == 4) {
                player4Troop = player4Troop - amount;
            }
        }

        public void update(DataSnapshot snapshot) {
            roomID = snapshot.child("roomID").getValue(String.class);
            winner = snapshot.child("winner").getValue(long.class);
            turn = snapshot.child("turn").getValue(long.class);
            trait1 = snapshot.child("trait1").getValue(String.class);
            trait2 = snapshot.child("trait2").getValue(String.class);
            trait3 = snapshot.child("trait3").getValue(String.class);
            trait4 = snapshot.child("trait4").getValue(String.class);
            class1 = snapshot.child("class1").getValue(String.class);
            class2 = snapshot.child("class2").getValue(String.class);
            class3 = snapshot.child("class3").getValue(String.class);
            class4 = snapshot.child("class4").getValue(String.class);
            player1Troop = snapshot.child("player1Troop").getValue(long.class);
            player2Troop = snapshot.child("player2Troop").getValue(long.class);
            player3Troop = snapshot.child("player3Troop").getValue(long.class);
            player4Troop = snapshot.child("player4Troop").getValue(long.class);
            player1action = snapshot.child("player1action").getValue(String.class);
            player2action = snapshot.child("player2action").getValue(String.class);
            player3action = snapshot.child("player3action").getValue(String.class);
            player4action = snapshot.child("player4action").getValue(String.class);
            player1Result = snapshot.child("player1Result").getValue(String.class);
            player2Result = snapshot.child("player2Result").getValue(String.class);
            player3Result = snapshot.child("player3Result").getValue(String.class);
            player4Result = snapshot.child("player4Result").getValue(String.class);
            resultAvailable = snapshot.child("resultAvailable").getValue(boolean.class);
        }

        public void upload() {
            Firebase roomRef = new Firebase("https://koalaminute.firebaseio.com/Games/"+roomID+"/");
            roomRef.child("roomID").setValue(roomID);
            roomRef.child("winner").setValue(winner);
            roomRef.child("turn").setValue(turn);
            roomRef.child("trait1").setValue(trait1);
            roomRef.child("trait2").setValue(trait2);
            roomRef.child("trait3").setValue(trait3);
            roomRef.child("trait4").setValue(trait4);
            roomRef.child("class1").setValue(class1);
            roomRef.child("class2").setValue(class2);
            roomRef.child("class3").setValue(class3);
            roomRef.child("class4").setValue(class4);
            roomRef.child("player1Troop").setValue(player1Troop);
            roomRef.child("player2Troop").setValue(player2Troop);
            roomRef.child("player3Troop").setValue(player3Troop);
            roomRef.child("player4Troop").setValue(player4Troop);
            roomRef.child("player1action").setValue(player1action);
            roomRef.child("player2action").setValue(player2action);
            roomRef.child("player3action").setValue(player3action);
            roomRef.child("player4action").setValue(player4action);
            roomRef.child("player1Result").setValue(player1Result);
            roomRef.child("player2Result").setValue(player2Result);
            roomRef.child("player3Result").setValue(player3Result);
            roomRef.child("player4Result").setValue(player4Result);
            roomRef.child("resultAvailable").setValue(resultAvailable);
        }
    }

    // Listener for GameRoomBattle information
    ValueEventListener roomListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            Animation slideIn = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_in_left);
            slideIn.setDuration(1000);
            Animation slideOut = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_out_right);
            slideOut.setDuration(1000);

            boolean serverGameOn = dataSnapshot.child("gameOn").getValue(boolean.class);
            boolean localGameOn = gameRoomBattle.isGameOn();

            // When game ends
            if ((localGameOn) && (!serverGameOn)) {
                //System.out.println("HERE");
                gameRoomBattle.setGameOff();
                return;
            }

            // Layout change when game starts
            if ((!localGameOn) && (serverGameOn)) {
                interfaceUpdate();
            }


            // Update local gameRoom
            gameRoomBattle.update(dataSnapshot);

            if (gameRoomBattle.getPlayer1() == null) {
                return;
            }

            // Check whether is able to start
            if (!gameRoomBattle.isGameOn()) {
                if (gameRoomBattle.ableToStart() && me == 1) {
                    message.setVisibility(View.INVISIBLE);
                    start.startAnimation(slideIn);
                    start.setVisibility(View.VISIBLE);
                } else if (me == 1){
                    String msg = "wait for others to ready";
                    message.setText(msg);
                    message.startAnimation(slideIn);
                    message.setVisibility(View.VISIBLE);
                }

                // Display name
                displayNameInitial();
            }
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {
            System.err.println("battleGen - roomListener failed");
        }
    };


    // Listen for GameBattle information
    ValueEventListener gameListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            String serverPlayer1action = dataSnapshot.child("player1action").getValue(String.class);
            String serverPlayer2action = dataSnapshot.child("player2action").getValue(String.class);
            String serverPlayer3action = dataSnapshot.child("player3action").getValue(String.class);
            String serverPlayer4action = dataSnapshot.child("player4action").getValue(String.class);
            Boolean serverResultAvailable = dataSnapshot.child("resultAvailable").getValue(boolean.class);

            // Update local gameBattle
            // If game is on, only update local when all players actions are uploaded
            if (gameRoomBattle.isGameOn()) {
                if (gameRoomBattle.getPlayerNum() == 2) {
                    if (!serverPlayer1action.equals("") && (!serverPlayer2action.equals(""))) {
                        gameBattle.update(dataSnapshot);
                        newTurnAvailable = true;
                    }
                } else if (gameRoomBattle.getPlayerNum() == 3) {
                    if (!serverPlayer1action.equals("") && (!serverPlayer2action.equals("")) &&
                            (!serverPlayer3action.equals(""))) {
                        gameBattle.update(dataSnapshot);
                        newTurnAvailable = true;
                    }
                } else if (gameRoomBattle.getPlayerNum() == 4) {
                    if (!serverPlayer1action.equals("") && (!serverPlayer2action.equals("")) &&
                            (!serverPlayer3action.equals("")) && (!serverPlayer4action.equals(""))) {
                        gameBattle.update(dataSnapshot);
                        newTurnAvailable = true;
                    }
                }
            }

            // If there are new results coming, update local data
            if (serverResultAvailable) {
                System.out.println("result available - update local gameBattle");
                gameBattle.update(dataSnapshot);
                gameBattle.setResultAvailableFalse();

                // Check whether there is a winner
                String winner = gameBattle.gameEnd();
                if (!winner.equals("")) {
                    System.out.println("winner: " + winner);
                    gameOver(winner);

                    // Remove the game
                    //firebaseRef.child("Games").child(gameRoomBattle.getRoomID()).setValue(null);
                    return;
                }

                printResult();
                gameBattle.removeAllResult();
                interfaceUpdateTurn();

                if (me == 1) {
                    firebaseRef.child("Games").child(gameRoomBattle.getRoomID()).setValue(gameBattle);
                }
            }

            // Only host will increase and upload turn/game information/operation at once
            if (me == 1 && newTurnAvailable) {
                newTurnAvailable = false;
                gameBattle.incrementTurn();

                // Perform action calculations and upload new game infomation
                performAction(serverPlayer1action, serverPlayer2action, serverPlayer3action, serverPlayer4action);
                System.out.println("me: " + me + " performAction");
                gameBattle.removeAllAction();
                gameBattle.setResultAvailableTrue();
                firebaseRef.child("Games").child(gameRoomBattle.getRoomID()).setValue(gameBattle);
            }

        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {
            System.err.println("battleGen - gameListener failed");
        }
    };


    // This method records user actions
    // RadioGroup - attackOrDefend button hits
    public void decideAttackOrDefend(View view) {
        Animation slideIn = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_in_left);
        slideIn.setDuration(1000);
        Animation slideOut = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_out_right);
        slideOut.setDuration(1000);

        // Get selected radio button from radioGroup
        int selectedId = radioGroup_attackOrDefend.getCheckedRadioButtonId();
        if (selectedId == -1) {
            Toast.makeText(getApplicationContext(), "Please choose attack or defend", Toast.LENGTH_LONG).show();
        } else {
            RadioButton radioButton_attackOrDefend = (RadioButton)findViewById(selectedId);
            String chosenText = radioButton_attackOrDefend.getText().toString();

            // Player chooses attack
            String attack = getString(R.string.attack);
            String defend = getString(R.string.defend);
            String produce = getString(R.string.produce);
            if (chosenText.equals(attack)) {
                //System.out.println("attack");
                gameBattle.appendAction(me, "attack");
                radioGroup_attackOrDefend.startAnimation(slideOut);
                radioGroup_attackOrDefend.setVisibility(View.INVISIBLE);

                if (gameRoomBattle.getPlayerNum() < 4) {
                    radio_player3.setVisibility(View.GONE);
                }

                updateRadioButtonPlayer();
                radioGroup_choosePlayer.startAnimation(slideIn);
                radioGroup_choosePlayer.setVisibility(View.VISIBLE);

                // Player chooses defend
            } else if (chosenText.equals(defend)) {
                //System.out.println("defend");
                gameBattle.appendAction(me, "defend");
                gameBattle.uploadAction(me);
                radioGroup_attackOrDefend.startAnimation(slideOut);
                radioGroup_attackOrDefend.setVisibility(View.INVISIBLE);

                String msg = "waiting for other players to make decision...";
                resultMessage.setText(msg);
                resultMessage.startAnimation(slideIn);
            } else if (chosenText.equals(produce)) {
                //System.out.println("defend");
                gameBattle.appendAction(me, "produce");
                gameBattle.uploadAction(me);
                radioGroup_attackOrDefend.startAnimation(slideOut);
                radioGroup_attackOrDefend.setVisibility(View.INVISIBLE);

                String msg = "waiting for other players to make decision...";
                resultMessage.setText(msg);
                resultMessage.startAnimation(slideIn);
            }
        }
    }

    // This method records user attacks which player
    public void decideChoosePlayer(View view) {
        Animation slideIn = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_in_left);
        slideIn.setDuration(1000);
        Animation slideOut = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_out_right);
        slideOut.setDuration(1000);

        // Get selected radio button from radioGroup
        int selectedId = radioGroup_choosePlayer.getCheckedRadioButtonId();
        if (selectedId == -1) {
            Toast.makeText(getApplicationContext(), "Please choose a player", Toast.LENGTH_LONG).show();
        } else {
            RadioButton radioButton_player = (RadioButton)findViewById(selectedId);
            String chosenText = radioButton_player.getText().toString();
            gameBattle.appendAction(me, " " + chosenText);
            gameBattle.uploadAction(me);

            radioGroup_choosePlayer.startAnimation(slideOut);
            radioGroup_choosePlayer.setVisibility(View.INVISIBLE);

            String msg = "wait for other players to make decision...";
            resultMessage.setText(msg);
            resultMessage.startAnimation(slideIn);
        }
    }
//this class is used to calculate battle result
    class BattleAbility{
        long type;
        long number;
        long attack_power;
        long defend_power;
        long produce_power;
        Random random;
        BattleAbility(long number, long type) {
            random = new Random();
            this.number = number;
            this.type = type;
            if(type == 0) {
                attack_power = number/5;
                defend_power = number/12;
                produce_power = number/12;
            }
        }

        public long defend(long enemy_power) {
            return (enemy_power-defend_power*2);
        }
    }

    // Calculate actions according to input
    private void performAction(String action1, String action2, String action3, String action4) {
        boolean player1Defend = false;
        boolean player2Defend = false;
        boolean player3Defend = false;
        boolean player4Defend = false;
        long player1troop = gameBattle.getPlayer1Troop();
        long player2troop = gameBattle.getPlayer2Troop();
        long player3troop = gameBattle.getPlayer3Troop();
        long player4troop = gameBattle.getPlayer4Troop();
        BattleAbility[] playerBattleList = new BattleAbility[4];
        playerBattleList[0] = new BattleAbility(player1troop, 0);
        playerBattleList[1] = new BattleAbility(player2troop, 0);
        playerBattleList[2] = new BattleAbility(player3troop, 0);
        playerBattleList[3] = new BattleAbility(player4troop, 0);


        if (action1.equals("defend")) {player1Defend = true;}
        if (action2.equals("defend")) {player2Defend = true;}
        if (action3.equals("defend")) {player3Defend = true;}
        if (action4.equals("defend")) {player4Defend = true;}

        String []action1Array = action1.split("\\s+");
        String []action2Array = action2.split("\\s+");
        String []action3Array = action3.split("\\s+");
        String []action4Array = action4.split("\\s+");
        if(action1.equals("produce")) {
            long gain = playerBattleList[1].produce_power;
            gameBattle.decrementTroop(1, -gain);
            gameBattle.appendResult(1, "You gained " + gain + " troops from production!");
        } if(action2.equals("produce")) {
            long gain = playerBattleList[2].produce_power;
            gameBattle.decrementTroop(2, -gain);
            gameBattle.appendResult(2, "You gained " + gain + " troops from production!");
        } if(action3.equals("produce")) {
            long gain = playerBattleList[3].produce_power;
            gameBattle.decrementTroop(3, -gain);
            gameBattle.appendResult(3, "You gained " + gain + " troops from production!");
        } if(action4.equals("produce")) {
            long gain = playerBattleList[4].produce_power;
            gameBattle.decrementTroop(4, -gain);
            gameBattle.appendResult(4, "You gained " + gain + " troops from production!");
        }

        if (action1.contains("attack")) {

            // Check which player that player1 is attacking
            int playerNum = gameRoomBattle.checkPlayerByName(action1Array[1]);

            String attackMessage = gameRoomBattle.getPlayer1() + " attacked you!";
            int attacking_player = 1;
            String attackMessageforme = "You made an attack, but the kingdom you attcked was defending!";

            gameBattle.appendResult(playerNum, attackMessage);
            long targetlost = playerBattleList[playerNum-1].defend(playerBattleList[attacking_player].attack_power);
            long melost = playerBattleList[playerNum-1].defend_power*2+1 - playerBattleList[attacking_player].defend_power;
            // If the player being attack is defending, double defence power
            if (playerNum == 1 && player1Defend) {
                gameBattle.decrementTroop(playerNum, targetlost);
            } else if (playerNum == 2 && player2Defend) {
                gameBattle.decrementTroop(playerNum, targetlost);
            } else if (playerNum == 3 && player3Defend) {
                gameBattle.decrementTroop(playerNum, targetlost);
            } else if (playerNum == 4 && player4Defend) {
                gameBattle.decrementTroop(playerNum, targetlost);
            } else {
                melost = playerBattleList[playerNum-1].defend_power+1 - playerBattleList[attacking_player].defend_power;
                targetlost = playerBattleList[attacking_player].attack_power-playerBattleList[playerNum-1].defend_power;
                gameBattle.decrementTroop(playerNum, targetlost);
                attackMessageforme = new String();
                attackMessageforme = "You made an attack.";

            }
            //update the attacking player
            gameBattle.decrementTroop(attacking_player, melost);
            gameBattle.appendResult(attacking_player, attackMessageforme);
            gameBattle.appendResult(attacking_player, "You lost " + melost + " troops during the attack!");

            //update attcked player
            gameBattle.appendResult(playerNum,"You lost " + targetlost + " troops when defending your home from " + gameRoomBattle.getPlayer1());
        }

        if (action2.contains("attack")) {
            // Check which player that player2 is attacking
            int playerNum = gameRoomBattle.checkPlayerByName(action2Array[1]);
            String attackMessage = gameRoomBattle.getPlayer2() + " attacked you!";
            int attacking_player = 2;
            String attackMessageforme = "You made an attack, but the kingdom you attcked was defending!";

            gameBattle.appendResult(playerNum, attackMessage);
            long targetlost = playerBattleList[playerNum-1].defend(playerBattleList[attacking_player].attack_power);
            long melost = playerBattleList[playerNum-1].defend_power*2+1 - playerBattleList[attacking_player].defend_power;
            // If the player being attack is defending, double defence power
            if (playerNum == 1 && player1Defend) {
                gameBattle.decrementTroop(playerNum, targetlost);
            } else if (playerNum == 2 && player2Defend) {
                gameBattle.decrementTroop(playerNum, targetlost);
            } else if (playerNum == 3 && player3Defend) {
                gameBattle.decrementTroop(playerNum, targetlost);
            } else if (playerNum == 4 && player4Defend) {
                gameBattle.decrementTroop(playerNum, targetlost);
            } else {
                melost = playerBattleList[playerNum-1].defend_power+1 - playerBattleList[attacking_player].defend_power;
                targetlost = playerBattleList[attacking_player].attack_power-playerBattleList[playerNum-1].defend_power;
                gameBattle.decrementTroop(playerNum, targetlost);
                attackMessageforme = new String();
                attackMessageforme = "You made an attack.";

            }
            //update the attacking player
            gameBattle.decrementTroop(attacking_player, melost);
            gameBattle.appendResult(attacking_player, attackMessageforme);
            gameBattle.appendResult(attacking_player, "You lost " + melost + " troops during the attack!");

            //update attcked player
            gameBattle.appendResult(playerNum,"You lost " + targetlost + " troops when defending your home from " + gameRoomBattle.getPlayer2());
        }

        if (action3.contains("attack")) {
            int playerNum = gameRoomBattle.checkPlayerByName(action3Array[1]);
            String attackMessage = gameRoomBattle.getPlayer3() + " attacked you!";
            int attacking_player = 3;
            String attackMessageforme = "You made an attack, but the kingdom you attcked was defending!";

            gameBattle.appendResult(playerNum, attackMessage);
            long targetlost = playerBattleList[playerNum-1].defend(playerBattleList[attacking_player].attack_power);
            long melost = playerBattleList[playerNum-1].defend_power*2+1 - playerBattleList[attacking_player].defend_power;
            // If the player being attack is defending, double defence power
            if (playerNum == 1 && player1Defend) {
                gameBattle.decrementTroop(playerNum, targetlost);
            } else if (playerNum == 2 && player2Defend) {
                gameBattle.decrementTroop(playerNum, targetlost);
            } else if (playerNum == 3 && player3Defend) {
                gameBattle.decrementTroop(playerNum, targetlost);
            } else if (playerNum == 4 && player4Defend) {
                gameBattle.decrementTroop(playerNum, targetlost);
            } else {
                melost = playerBattleList[playerNum-1].defend_power+1 - playerBattleList[attacking_player].defend_power;
                targetlost = playerBattleList[attacking_player].attack_power-playerBattleList[playerNum-1].defend_power;
                gameBattle.decrementTroop(playerNum, targetlost);
                attackMessageforme = new String();
                attackMessageforme = "You made an attack.";

            }
            //update the attacking player
            gameBattle.decrementTroop(attacking_player, melost);
            gameBattle.appendResult(attacking_player, attackMessageforme);
            gameBattle.appendResult(attacking_player, "You lost " + melost + " troops during the attack!");

            //update attcked player
            gameBattle.appendResult(playerNum,"You lost " + targetlost + " troops when defending your home from " + gameRoomBattle.getPlayer3());
        }
        if (action4.contains("attack")) {
            int playerNum = gameRoomBattle.checkPlayerByName(action4Array[1]);
            String attackMessage = gameRoomBattle.getPlayer4() + " attacked you!";
            gameBattle.appendResult(playerNum, attackMessage);
            int attacking_player = 4;
            String attackMessageforme = "You made an attack, but the kingdom you attcked was defending!";

            gameBattle.appendResult(playerNum, attackMessage);
            long targetlost = playerBattleList[playerNum-1].defend(playerBattleList[attacking_player].attack_power);
            long melost = playerBattleList[playerNum-1].defend_power*2+1 - playerBattleList[attacking_player].defend_power;
            // If the player being attack is defending, double defence power
            if (playerNum == 1 && player1Defend) {
                gameBattle.decrementTroop(playerNum, targetlost);
            } else if (playerNum == 2 && player2Defend) {
                gameBattle.decrementTroop(playerNum, targetlost);
            } else if (playerNum == 3 && player3Defend) {
                gameBattle.decrementTroop(playerNum, targetlost);
            } else if (playerNum == 4 && player4Defend) {
                gameBattle.decrementTroop(playerNum, targetlost);
            } else {
                melost = playerBattleList[playerNum-1].defend_power+1 - playerBattleList[attacking_player].defend_power;
                targetlost = playerBattleList[attacking_player].attack_power-playerBattleList[playerNum-1].defend_power;
                gameBattle.decrementTroop(playerNum, targetlost);
                attackMessageforme = new String();
                attackMessageforme = "You made an attack.";

            }
            //update the attacking player
            gameBattle.decrementTroop(attacking_player, melost);
            gameBattle.appendResult(attacking_player, attackMessageforme);
            gameBattle.appendResult(attacking_player, "You lost " + melost + " troops during the attack!");

            //update attcked player
            gameBattle.appendResult(playerNum,"You lost " + targetlost + " troops when defending your home from " + gameRoomBattle.getPlayer3());


        }
/*
        if (action4.contains("attack")) {
            int playerNum = gameRoomBattle.checkPlayerByName(action4Array[1]);
            String attackMessage = gameRoomBattle.getPlayer4() + " attacked you!";
            gameBattle.appendResult(playerNum, attackMessage);

            // If the player being attack is defending, decrement halves
            if (playerNum == 1 && player1Defend) {
                gameBattle.decrementTroop(playerNum, baseDecrement/2);
            } else if (playerNum == 2 && player2Defend) {
                gameBattle.decrementTroop(playerNum, baseDecrement/2);
            } else if (playerNum == 3 && player3Defend) {
                gameBattle.decrementTroop(playerNum, baseDecrement/2);
            } else if (playerNum == 4 && player4Defend) {
                gameBattle.decrementTroop(playerNum, baseDecrement/2);
            } else {
                gameBattle.decrementTroop(playerNum, baseDecrement);
            }
        }
        */
    }

    // Print result after each turn
    private void printResult() {
        if (me == 1) {
            resultMessage.setText(gameBattle.getPlayer1Result());
            System.out.println("Result1: " + gameBattle.getPlayer1Result());
        } else if (me == 2) {
            resultMessage.setText(gameBattle.getPlayer2Result());
            System.out.println("Result2: " + gameBattle.getPlayer2Result());
        } else if (me == 3) {
            resultMessage.setText(gameBattle.getPlayer3Result());
            System.out.println("Result3: " + gameBattle.getPlayer3Result());
        } else if (me == 4) {
            resultMessage.setText(gameBattle.getPlayer4Result());
            System.out.println("Result4: " + gameBattle.getPlayer4Result());
        }

        Animation slideIn = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_in_left);
        slideIn.setDuration(1000);
        resultMessage.startAnimation(slideIn);
        resultMessage.setVisibility(View.VISIBLE);
    }

    // This is called when new player enters the room
    private void displayNameInitial() {
        if (!gameRoomBattle.getPlayer1().equals("")) {
            player1Name.setText(gameRoomBattle.getPlayer1());
        }
        if (!gameRoomBattle.getPlayer2().equals("")) {
            player2Name.setText(gameRoomBattle.getPlayer2());
        }
        if (!gameRoomBattle.getPlayer3().equals("")) {
            player3Name.setText(gameRoomBattle.getPlayer3());
        }
        if (!gameRoomBattle.getPlayer4().equals("")) {
            player4Name.setText(gameRoomBattle.getPlayer4());
        }
    }

    // Display name and troop after each turn
    private void displayNameAndTroop() {
        System.out.println("displayNameAndTroop");
        if (me == 1) {
            String display = gameRoomBattle.getPlayer1() + " - Troop: " + gameBattle.getPlayer1Troop();
            player1Name.setText(display);
            player2Name.setText(gameRoomBattle.getPlayer2());
            player3Name.setText(gameRoomBattle.getPlayer3());
            player4Name.setText(gameRoomBattle.getPlayer4());
        } else if (me == 2) {
            String display = gameRoomBattle.getPlayer2() + " - Troop: " + gameBattle.getPlayer2Troop();
            player2Name.setText(display);
            player1Name.setText(gameRoomBattle.getPlayer1());
            player3Name.setText(gameRoomBattle.getPlayer3());
            player4Name.setText(gameRoomBattle.getPlayer4());
        } else if (me == 3) {
            String display = gameRoomBattle.getPlayer3() + " - Troop: " + gameBattle.getPlayer3Troop();
            player3Name.setText(display);
            player1Name.setText(gameRoomBattle.getPlayer1());
            player2Name.setText(gameRoomBattle.getPlayer2());
            player4Name.setText(gameRoomBattle.getPlayer4());
        } else if (me == 4) {
            String display = gameRoomBattle.getPlayer4() + " - Troop: " + gameBattle.getPlayer4Troop();
            player4Name.setText(display);
            player1Name.setText(gameRoomBattle.getPlayer1());
            player2Name.setText(gameRoomBattle.getPlayer2());
            player3Name.setText(gameRoomBattle.getPlayer3());
        }
    }

    private void displayNameAndAllTroop() {
        String display = gameRoomBattle.getPlayer1() + " - Troop: " + gameBattle.getPlayer1Troop();
        player1Name.setText(display);
        display = gameRoomBattle.getPlayer2() + " - Troop: " + gameBattle.getPlayer2Troop();
        player2Name.setText(display);
        display = gameRoomBattle.getPlayer3() + " - Troop: " + gameBattle.getPlayer3Troop();
        player3Name.setText(display);
        display = gameRoomBattle.getPlayer4() + " - Troop: " + gameBattle.getPlayer4Troop();
        player4Name.setText(display);
    }

    // Layout change when game starts
    private void interfaceUpdate() {
        Animation slideIn = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_in_left);
        slideIn.setDuration(1000);
        Animation slideOut = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_out_right);
        slideOut.setDuration(1000);

        roomNumber.startAnimation(slideOut);
        roomNumber.setVisibility(View.INVISIBLE);
        start.startAnimation(slideOut);
        start.setVisibility(View.INVISIBLE);
        message.startAnimation(slideOut);
        message.setVisibility(View.INVISIBLE);

        displayNameAndTroop();

        String msg = "Turn: " + gameBattle.getTurn();
        gameMessage.setText(msg);
        gameMessage.startAnimation(slideIn);
        gameMessage.setVisibility(View.VISIBLE);
        radioGroup_attackOrDefend.startAnimation(slideIn);
        radioGroup_attackOrDefend.setVisibility(View.VISIBLE);

        // Make it invisible if the player is not available
        if (gameRoomBattle.getPlayer2Status() == 0) {
            player2Name.startAnimation(slideOut);
            player2Name.setVisibility(View.INVISIBLE);
        }
        if (gameRoomBattle.getPlayer3Status() == 0) {
            player3Name.startAnimation(slideOut);
            player3Name.setVisibility(View.INVISIBLE);
        }
        if (gameRoomBattle.getPlayer4Status() == 0) {
            player4Name.startAnimation(slideOut);
            player4Name.setVisibility(View.INVISIBLE);
        }
    }

    // Layout change when each turn ends
    private void interfaceUpdateTurn() {
        Animation slideIn = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_in_left);
        slideIn.setDuration(1000);
        Animation slideOut = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_out_right);
        slideOut.setDuration(1000);

        displayNameAndTroop();

        String msg = "Turn: " + gameBattle.getTurn();
        gameMessage.setText(msg);
        gameMessage.startAnimation(slideIn);
        gameMessage.setVisibility(View.VISIBLE);
        radioGroup_attackOrDefend.startAnimation(slideIn);
        radioGroup_attackOrDefend.setVisibility(View.VISIBLE);
    }


    // Game is over
    private void gameOver(String winner) {
        Animation slideIn = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_in_left);
        slideIn.setDuration(1000);
        Animation slideOut = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_out_right);
        slideOut.setDuration(1000);

        displayNameAndAllTroop();

        firebaseRef.child("Rooms").child(gameRoomBattle.getRoomID()).child("gameOn").setValue(false);

        radioGroup_attackOrDefend.startAnimation(slideOut);
        radioGroup_attackOrDefend.setVisibility(View.INVISIBLE);

        String msg = winner + " wins!";
        gameMessage.setText(msg);
        gameMessage.startAnimation(slideIn);

        // start button will only reappear at host
        if (me == 1) {
            start.startAnimation(slideIn);
            start.setVisibility(View.VISIBLE);
            resultMessage.startAnimation(slideOut);
            resultMessage.setVisibility(View.INVISIBLE);
        } else {
            msg = "waiting " + gameRoomBattle.getPlayer1() + " to restart or leave...";
            resultMessage.setText(msg);
            resultMessage.startAnimation(slideIn);

            // Reset gameBattle for players other than host
            gameBattle.setTurn(1);
            gameBattle.resetTroop();
        }
    }


    // Update the choices according to different player
    private void updateRadioButtonPlayer() {
        if (me == 1) {
            radio_player1.setText(gameRoomBattle.getPlayer2());
            radio_player2.setText(gameRoomBattle.getPlayer3());
            radio_player3.setText(gameRoomBattle.getPlayer4());
        } else if (me == 2) {
            radio_player1.setText(gameRoomBattle.getPlayer1());
            radio_player2.setText(gameRoomBattle.getPlayer3());
            radio_player3.setText(gameRoomBattle.getPlayer4());
        } else if (me == 3) {
            radio_player1.setText(gameRoomBattle.getPlayer1());
            radio_player2.setText(gameRoomBattle.getPlayer2());
            radio_player3.setText(gameRoomBattle.getPlayer4());
        } else if (me == 4) {
            radio_player1.setText(gameRoomBattle.getPlayer1());
            radio_player2.setText(gameRoomBattle.getPlayer2());
            radio_player3.setText(gameRoomBattle.getPlayer3());
        }
    }

    // Button - Ready hits
    public void battleReady(View view) {
        Animation slideIn = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_in_left);
        slideIn.setDuration(1000);
        Animation slideOut = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_out_right);
        slideOut.setDuration(1000);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            final Button readyButton = (Button) findViewById(R.id.ready);
            // get the center for the clipping circle
            int cx = readyButton.getWidth() / 2;
            int cy = readyButton.getHeight() / 2;
            // get the initial radius for the clipping circle
            float initialRadius = (float) Math.hypot(cx, cy);

            // create the animation (the final radius is zero)
            Animator anim =
                    ViewAnimationUtils.createCircularReveal(readyButton, cx, cy, initialRadius, 0);
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    readyButton.setVisibility(View.INVISIBLE);
                }
            });
            anim.start();
        } else {
            ready.startAnimation(slideOut);
            ready.setVisibility(View.INVISIBLE);
        }


        String msg = "Wait for " + gameRoomBattle.getPlayer1() + " to start the game...";
        message.setText(msg);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int cx = message.getWidth() / 2;
            int cy = message.getHeight() / 2;
            float finalRadius = (float) Math.hypot(cx, cy);
            // create the animator for this view (the start radius is zero)
            Animator anim =
                    ViewAnimationUtils.createCircularReveal(message, cx, cy, 0, finalRadius);
            message.setVisibility(View.VISIBLE);
            anim.start();
        } else {
            message.startAnimation(slideIn);
            message.setVisibility(View.VISIBLE);
        }

        gameRoomBattle.setPlayerStatus(me, 2);
        gameRoomBattle.uploadPlayerStatus(me, 2);

        gameBattle = new GameBattle(gameRoomBattle.getRoomID());
        firebaseRef.child("Games").child(gameRoomBattle.getRoomID()).addValueEventListener(gameListener);
    }

    // Button - Start hits
    public void battleStart(View view) {
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_out_right);
        animation.setDuration(1000);
        start.startAnimation(animation);
        start.setVisibility(View.INVISIBLE);

        firebaseRef.child("Rooms").child(gameRoomBattle.getRoomID()).child("gameOn").setValue(true);
        gameBattle = new GameBattle(gameRoomBattle.getRoomID());
        firebaseRef.child("Games").child(gameRoomBattle.getRoomID()).setValue(gameBattle);
        firebaseRef.child("Games").child(gameRoomBattle.getRoomID()).addValueEventListener(gameListener);
    }


    // Button - Leave hits
    public void battleLeave(View view) {
        firebaseRef.child("Games").child(gameRoomBattle.getRoomID()).removeEventListener(gameListener);
        firebaseRef.child("Games").child(gameRoomBattle.getRoomID()).removeEventListener(roomListener);
        gameRoomBattle.decrementPlayerNum();

        if (gameRoomBattle.getPlayerNum() == 1) {
            firebaseRef.child("Rooms").child(gameRoomBattle.getRoomID()).setValue(gameRoomBattle);
        } else {
            firebaseRef.child("Rooms").child(gameRoomBattle.getRoomID()).setValue(null);
            if (gameRoomBattle.isGameOn()) {
                firebaseRef.child("Games").child(gameRoomBattle.getRoomID()).setValue(null);
            }
        }

        finish();
    }


    public void testAnimate(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            final Button testButton = (Button) findViewById(R.id.ready);
            // get the center for the clipping circle
            int cx = testButton.getWidth() / 2;
            int cy = testButton.getHeight() / 2;
            // get the initial radius for the clipping circle
            float initialRadius = (float) Math.hypot(cx, cy);

            // create the animation (the final radius is zero)
            Animator anim =
                    ViewAnimationUtils.createCircularReveal(testButton, cx, cy, initialRadius, 0);
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    testButton.setVisibility(View.INVISIBLE);
                }
            });
            anim.start();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_room_battle_gen);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        // Widgets
        roomNumber = (TextView) findViewById(R.id.roomNumber);
        player1Name = (TextView) findViewById(R.id.player1Name);
        player2Name = (TextView) findViewById(R.id.player2Name);
        player3Name = (TextView) findViewById(R.id.player3Name);
        player4Name = (TextView) findViewById(R.id.player4Name);
        message = (TextView) findViewById(R.id.message);
        gameMessage = (TextView) findViewById(R.id.gameMessage);
        resultMessage = (TextView) findViewById(R.id.resultMessage);
        start = (Button) findViewById(R.id.start);
        ready = (Button) findViewById(R.id.ready);
        attackOrDefend = (Button) findViewById(R.id.radio_decision_attackOrDefend);
        radioGroup_attackOrDefend = (RadioGroup) findViewById(R.id.radioGroup_attackOrDefend);
        radioGroup_choosePlayer = (RadioGroup) findViewById(R.id.radioGroup_choosePlayer);
        radio_player1 = (RadioButton) findViewById(R.id.radio_player1);
        radio_player2 = (RadioButton) findViewById(R.id.radio_player2);
        radio_player3 = (RadioButton) findViewById(R.id.radio_player3);

        firebaseRef = new Firebase("https://koalaminute.firebaseio.com/");
        AuthData authData = firebaseRef.getAuth();
        if (authData != null) {
            UID = authData.getUid();
            System.out.println("UID: " + UID);
        } else {
            System.err.println("No authData in GameRoomLiarGen");
            finish();
        }


        // Get room id from passing parameter and display name from sharePreference
        final Bundle bundleRoomID = getIntent().getExtras();
        SharedPreferences sharedPreferences = this.getSharedPreferences("koala.minute", Context.MODE_PRIVATE);
        displayName = sharedPreferences.getString("displayName","");

        // If the player is creating a new room
        if (bundleRoomID == null) {
            me = 1;
            gameRoomBattle = new GameRoomBattle(UID);

            ValueEventListener createRoom = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String currentRoomID = dataSnapshot.getValue(String.class);
                    String newRoomID = Integer.toString(Integer.parseInt(currentRoomID) + 1);
                    firebaseRef.child("RoomIDcounter").setValue(newRoomID);
                    gameRoomBattle.setRoomID(newRoomID);
                    gameRoomBattle.incrementPlayerNum();
                    gameRoomBattle.setPlayerName(me, displayName);
                    gameRoomBattle.setPlayerID(me, UID);
                    gameRoomBattle.setPlayerStatus(me, 1);
                    gameRoomBattle.upload();
                    firebaseRef.child("Rooms").child(gameRoomBattle.getRoomID()).addValueEventListener(roomListener);

                    // Display roomID
                    String roomIDMessage = "Room: " + gameRoomBattle.getRoomID();
                    roomNumber.setText(roomIDMessage);
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {
                    System.err.println("battleGen - createRoom listener failed");
                }
            };

            // Apply this listener
            firebaseRef.child("RoomIDcounter").addListenerForSingleValueEvent(createRoom);


            // If the player is entering the room
        } else {
            String roomID = bundleRoomID.getString("ID");
            ready.setVisibility(View.VISIBLE);
            gameRoomBattle = new GameRoomBattle();
            ValueEventListener joinRoom = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    gameRoomBattle.update(dataSnapshot);
                    gameRoomBattle.incrementPlayerNum();
                    me = gameRoomBattle.nextAvailable();
        System.out.println("me: " + me);
                    gameRoomBattle.setPlayerName(me, displayName);
                    gameRoomBattle.setPlayerID(me, UID);
                    gameRoomBattle.setPlayerStatus(me, 1);
                    gameRoomBattle.upload();
                    firebaseRef.child("Rooms").child(gameRoomBattle.getRoomID()).addValueEventListener(roomListener);

                    // Display roomID
                    String roomIDMessage = "Room: " + gameRoomBattle.getRoomID();
                    roomNumber.setText(roomIDMessage);
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {
                    System.err.println("battleGen - joinRoom listener failed");
                }
            };

            // Apply this listener
            firebaseRef.child("Rooms").child(roomID).addListenerForSingleValueEvent(joinRoom);
        }


    }

}
