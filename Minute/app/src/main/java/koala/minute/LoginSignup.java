//logo was created on https://www.graphicsprings.com/start-your-logo
package koala.minute;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ActivityOptions;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.Map;

public class LoginSignup extends AppCompatActivity implements DialogFragmentDisplayName.NoticeDialogListener{
    private Animation animationtrans;
    private Animation buttonerror;
    Firebase firebaseRef;
    EditText userNameField;
    EditText passwordField;
    EditText emailAddressField;
    boolean newUser = false;
    String userName;
    String passWord;
    SharedPreferences sharedPreferences;
    private View login_view;

    // Create a handler to handle the result of the authentication
    Firebase.AuthResultHandler authResultHandler = new Firebase.AuthResultHandler() {
        @Override
        public void onAuthenticated(AuthData authData) {
            System.out.println("login success");
            sharedPreferences.edit().putString("username", userName).apply();
            sharedPreferences.edit().putString("password", passWord).apply();
            if(!newUser) {
                transToHomePage();
            }
        }

        @Override
        public void onAuthenticationError(FirebaseError firebaseError) {
            if(login_view != null){
                login_view.startAnimation(buttonerror);
            }
            displayErrorMessage(firebaseError);
        }
    };


    Firebase.ResultHandler resultHandlerResetPassword = new Firebase.ResultHandler() {
        @Override
        public void onSuccess() {
            Toast.makeText(getApplicationContext(), "Email sent.", Toast.LENGTH_LONG).show();
            final RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);
            final RelativeLayout emailLayout = (RelativeLayout) findViewById(R.id.emailLayout);
            mainLayout.setVisibility(View.VISIBLE);
            emailLayout.setVisibility(View.GONE);

        }

        @Override
        public void onError(FirebaseError firebaseError) {
            displayErrorMessage(firebaseError);
        }
    };

    private void transToHomePage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setExitTransition(new Explode());
            Intent intent = new Intent(this, Splash.class);
            startActivity(intent,
                    ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else {
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
        }
    }


    // Called when error happens in result handler
    private void displayErrorMessage(FirebaseError firebaseError) {
        switch (firebaseError.getCode()) {
            case FirebaseError.USER_DOES_NOT_EXIST:
                Toast.makeText(getApplicationContext(), "User account does not exist.", Toast.LENGTH_LONG).show();
                break;
            case FirebaseError.INVALID_PASSWORD:
                Toast.makeText(getApplicationContext(), "Password is incorrect.", Toast.LENGTH_LONG).show();
                break;
            case FirebaseError.INVALID_EMAIL:
                Toast.makeText(getApplicationContext(), "The specified email is not a valid email.", Toast.LENGTH_LONG).show();
                break;
            case FirebaseError.NETWORK_ERROR:
                Toast.makeText(getApplicationContext(), "An error occurred while attempting to contact the authentication server.", Toast.LENGTH_LONG).show();
                break;
            default:
                Toast.makeText(getApplicationContext(), "Something goes wrong", Toast.LENGTH_LONG).show();
                break;
        }
    }


    // Create an instance of the dialog fragment and show it
    public void showDisplayNameDialog() {
        DialogFragment dialog = new DialogFragmentDisplayName();
        dialog.show(getFragmentManager(), "DisplayNameFragment");
    }

    // Implement the interface - DialogFragmentDisplayName.NoticeDialogListener
    // The dialog fragment receives a reference to this Activity through the
    // Fragment.onAttach() callback, which it uses to call the following methods
    // defined by the NoticeDialogFragment.NoticeDialogListener interface
    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        Intent i = new Intent(getApplicationContext(), HomePage.class);
        startActivity(i);
    }


    // Sign up
    public void Signup(final View view) {

        userName = String.valueOf(userNameField.getText());
        passWord = String.valueOf(passwordField.getText());
        //

        // Save into shared preferences for now


        firebaseRef.createUser(userName, passWord, new Firebase.ValueResultHandler<Map<String, Object>>() {

            @Override
            public void onSuccess(Map<String, Object> result) {
                System.out.println("Successfully created user account with uid: " + result.get("uid"));
                view.startAnimation(animationtrans);
                // Pop-up; ask user to input display name
                //showDisplayNameDialog();
                final String UID = (String) result.get("uid");
                ValueEventListener listenID = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        // Retrieve IDcounter from server and increment it by 1
                        String idString;
                        idString = snapshot.getValue(String.class);
                        String myID = Integer.toString(Integer.parseInt(idString) + 1);
                        // Save the updated IDcounter on server
                        firebaseRef.child("IDcounter").setValue(myID);

                        // Create user and save it on server
                        User user = new User(myID, userName);
                        firebaseRef.child("Users").child(UID).setValue(user);
                        firebaseRef.child("UserIDMap").child(myID).setValue(UID);
                    }


                    @Override
                    public void onCancelled(FirebaseError firebaseError) {
                        System.out.println("User/UserID create failed");
                    }
                };

                firebaseRef.child("IDcounter").addListenerForSingleValueEvent(listenID);
                firebaseRef.child("IDcounter").removeEventListener(listenID);

                //getSharedPreferences("userinfo",0);
                // Login the user  set user name
                newUser = true;
                userName = String.valueOf(userNameField.getText());
                passWord = String.valueOf(passwordField.getText());
                firebaseRef.authWithPassword(userName, passWord, authResultHandler);
                showDisplayNameDialog();
                //logIn(view);
            }

            @Override
            public void onError(FirebaseError firebaseError) {
                view.startAnimation(buttonerror);
                displayErrorMessage(firebaseError);
            }
        });
    }

    public void logIn(View view) {
        login_view = view;

        userName = String.valueOf(userNameField.getText());
        passWord = String.valueOf(passwordField.getText());

        firebaseRef.authWithPassword(userName, passWord, authResultHandler);
        //view.startAnimation(animationtrans);
        view.setVisibility(View.INVISIBLE);
        view.postDelayed(new Runnable() {
            @Override
            public void run() {
                login_view.setVisibility(View.VISIBLE);
            }
        },100);
    }


    public void swapIntoEmailLayout(View view) {
        final RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);
        final RelativeLayout emailLayout = (RelativeLayout) findViewById(R.id.emailLayout);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int cx = mainLayout.getWidth() / 2;
            int cy = mainLayout.getHeight() / 2;
            float initialRadius = (float) Math.hypot(cx, cy);

            // create the animation (the final radius is zero)
            Animator anim =
                    ViewAnimationUtils.createCircularReveal(mainLayout, cx, cy, initialRadius, 0);
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mainLayout.setVisibility(View.GONE);
                }
            });
            anim.start();

            cx = emailLayout.getWidth() / 2;
            cy = emailLayout.getHeight() / 2;
            float finalRadius = (float) Math.hypot(cx, cy);
            // create the animator for this view (the start radius is zero)
            anim =
                    ViewAnimationUtils.createCircularReveal(emailLayout, cx, cy, 0, finalRadius);
            emailLayout.setVisibility(View.VISIBLE);
            anim.start();

        } else {
            mainLayout.setVisibility(View.GONE);
            emailLayout.setVisibility(View.VISIBLE);
        }
    }


    public void swapIntoMainLayout(View view) {
        final RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);
        final RelativeLayout emailLayout = (RelativeLayout) findViewById(R.id.emailLayout);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int cx = emailLayout.getWidth() / 2;
            int cy = emailLayout.getHeight() / 2;
            float initialRadius = (float) Math.hypot(cx, cy);

            // create the animation (the final radius is zero)
            Animator anim =
                    ViewAnimationUtils.createCircularReveal(emailLayout, cx, cy, initialRadius, 0);
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    emailLayout.setVisibility(View.GONE);
                }
            });
            anim.start();

            cx = mainLayout.getWidth() / 2;
            cy = mainLayout.getHeight() / 2;
            float finalRadius = (float) Math.hypot(cx, cy);
            // create the animator for this view (the start radius is zero)
            anim =
                    ViewAnimationUtils.createCircularReveal(mainLayout, cx, cy, 0, finalRadius);
            mainLayout.setVisibility(View.VISIBLE);
            anim.start();

        } else {
            emailLayout.setVisibility(View.GONE);
            mainLayout.setVisibility(View.VISIBLE);
        }
    }


    public void resetPassword(View view) {
        String emailAddress = String.valueOf(emailAddressField.getText());

        if (emailAddress.equals("")) {
            Toast.makeText(getApplicationContext(), "Please enter an email.", Toast.LENGTH_LONG).show();
        } else {
            firebaseRef.resetPassword(emailAddress, resultHandlerResetPassword);
        }
    }

    /*@Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);}
        // getActivity().getWindow().getDecorView().setSystemUiVisibility(newUiOptions);
    }*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Enable transitions if system version is at least API21
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        }

        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);
        // button animation
        animationtrans = new AnimationUtils().loadAnimation(this,R.anim.anim_trans_login);
        buttonerror = new AnimationUtils().loadAnimation(this,R.anim.mybutton_error);
        //--------
        setContentView(R.layout.activity_login_signup);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        // Declarations
        userNameField = (EditText)findViewById(R.id.userName);
        passwordField = (EditText)findViewById(R.id.passWord);
        emailAddressField = (EditText)findViewById(R.id.emailAddress);
        firebaseRef = new Firebase("https://koalaminute.firebaseio.com/");

        sharedPreferences = this.getSharedPreferences("koala.minute", Context.MODE_PRIVATE);
        String username = sharedPreferences.getString("username", "");
        String password = sharedPreferences.getString("password", "");
        System.out.println("aaa" + username);
        if (username != null && username != "") {
            firebaseRef.authWithPassword(username, password, authResultHandler);
            // Intent i = new Intent(getApplicationContext(), HomePage.class);
            // startActivity(i);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login_signup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
