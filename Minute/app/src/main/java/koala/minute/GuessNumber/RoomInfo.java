package koala.minute.GuessNumber;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.EventListener;

/**
 * Created by Administrator on 2016/2/28.
 * this class is used to automatically update the information of the GuessNumber room
 *
 */
public class RoomInfo {
    String roomID;
    Firebase firebaseRef;
    ValueEventListener roomListener;
    RoomInfo(String roomID) {
        this.roomID = roomID;
        firebaseRef = new Firebase("https://koalaminute.firebaseio.com/Rooms/").child(roomID);

    }
    void createListener() {
        roomListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                String player1 = snapshot.child("player1").getValue(String.class);
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {
                System.out.println("The read failed: " + firebaseError.getMessage());
            }
        };

    }

    public int BeginListen() {



        return 1;
    }

}
