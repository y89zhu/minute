package koala.minute;
// background image: https://pixabay.com/en/wordpress-background-web-design-581849/
// edited for non-commercial use
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

public class LiarHomePage extends AppCompatActivity implements DialogFragmentDisplayName.NoticeDialogListener{
    private Animation animationtrans;
    Firebase firebaseRef;
    TextView description;
    String UID;
    SharedPreferences sharedPreferences;


    public void changeDisplayName (View view) {
        DialogFragment dialog = new DialogFragmentDisplayName();
        dialog.show(getFragmentManager(), "DisplayNameFragment");
    }

    public void createLiarGame(View view) {
        view.startAnimation(animationtrans);
        Intent intent = new Intent(this, GameRoomLiarGen.class);
        startActivity(intent);
    }

    public void joinLairGame(View view){
        view.startAnimation(animationtrans);
        EditText room_id = (EditText) findViewById(R.id.inputMax);
        final String parseId = room_id.getText().toString();
        room_id.setText("");

        if (parseId.equals("")) {
            Toast.makeText(getApplicationContext(), "Invalid Game Room ID.", Toast.LENGTH_LONG).show();
        } else {
            final Intent intent = new Intent(this, GameRoomLiarGen.class);

            // Pass the roomID to new activity

            ValueEventListener createRoom = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    if (snapshot.getValue() == null) {
                        Toast.makeText(getApplicationContext(), "Invalid Game Room ID.", Toast.LENGTH_LONG).show();
                        return;
                    }
                    Bundle RoomID = new Bundle();
                    RoomID.putString("ID", parseId);
                    intent.putExtras(RoomID);
                    startActivity(intent);
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {
                    System.out.println("join room cancelld");
                }
            };
            firebaseRef.child("Rooms").child(parseId).addListenerForSingleValueEvent(createRoom);
            firebaseRef.removeEventListener(createRoom);
        }
    }


    public void swapIntoJoinLayoutLiar(View view) {
        final RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.mainLayoutLiar);
        final RelativeLayout joinLayout = (RelativeLayout) findViewById(R.id.joinLayoutLiar);

        Animation slideIn = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_in_left);
        Animation slideOut = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_out_right);
        slideIn.setDuration(300);
        slideOut.setDuration(300);

        mainLayout.startAnimation(slideOut);
        mainLayout.setVisibility(View.GONE);
        joinLayout.startAnimation(slideIn);
        joinLayout.setVisibility(View.VISIBLE);
    }


    public void swapIntoMainLayoutLiar(View view) {
        final RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.mainLayoutLiar);
        final RelativeLayout joinLayout = (RelativeLayout) findViewById(R.id.joinLayoutLiar);

        Animation slideIn = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_in_left);
        Animation slideOut = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_out_right);
        slideIn.setDuration(300);
        slideOut.setDuration(300);

        joinLayout.startAnimation(slideOut);
        joinLayout.setVisibility(View.GONE);
        mainLayout.startAnimation(slideIn);
        mainLayout.setVisibility(View.VISIBLE);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_liar_home_page);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        animationtrans = new AnimationUtils().loadAnimation(this,R.anim.anim_translate);

        firebaseRef = new Firebase("https://koalaminute.firebaseio.com/");
        description = (TextView) findViewById(R.id.descriptionGuess);
        description.setMovementMethod(new ScrollingMovementMethod());
        sharedPreferences = this.getSharedPreferences("koala.minute", Context.MODE_PRIVATE);

        AuthData authData = firebaseRef.getAuth();
        if (authData != null) {
            UID = authData.getUid();
            System.out.println("UID: " + UID);
        } else {
            System.out.println("No authData in HomePage");
        }
        if(UID == null) {
            System.out.println("home page no auth, wh");
            firebaseRef.unauth();
            Intent intent = new Intent(this, LoginSignup.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return;
        }
    }


    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {

    }
}
