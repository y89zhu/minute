package koala.minute;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import java.util.Random;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Thread splash_screen = new Thread(){
            public void run(){
                try{
                    Random a = new Random();
                    Integer b = a.nextInt(200)+500;
                    sleep(b);
                }catch(Exception e){
                    System.out.println(e);
                }finally {
                    startActivity(new Intent(getApplicationContext(),HomePage.class));
                    finish();
                }
            }
        };
        splash_screen.start();
    }

}
