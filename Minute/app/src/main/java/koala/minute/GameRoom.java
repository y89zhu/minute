package koala.minute;

public class GameRoom {
    private String RoomID;
    private String player1;
    private String player2;
    private String player1ID;
    private String player2ID;
    private String gameHolderID;
    private long playerNum;
    public boolean gameOn;
    private long player2Status;

    public GameRoom() {}

    public GameRoom(String gameHolderID) {
        this.gameHolderID = gameHolderID;
        this.player1 = "";
        this.player2 = "waiting";
        this.player1ID = gameHolderID;
        this.player2ID = "0";
        this.gameOn = false;
        this.playerNum = 0;
        // status: 0:not available 1:not ready 2:ready
        this.player2Status = 0;
    }

    public String getRoomID() {return RoomID;}
    public void setRoomID(String ID) {RoomID = ID;}

    public void setPlayer1(String player1) {this.player1 = player1;}
    public void setPlayer2(String player2) {this.player2 = player2;}
    public String getPlayer1() {return player1;}
    public String getPlayer2() {return player2;}

    public void setPlayer1ID(String player1ID) {this.player1ID = player1ID;}
    public void setPlayer2ID(String player2ID) {this.player2ID = player2ID;}
    public String getPlayer1ID() {return player1ID;}
    public String getPlayer2ID() {return player2ID;}

    public String getGameHolderID() {return gameHolderID;}
    public void setGameHolderID(String gameHolderID) {this.gameHolderID= gameHolderID;}

    public long getPlayerNum() {return playerNum;}
    public void setPlayerNum(long playerNum) {this.playerNum = playerNum;}
    public void incrementPlayerNum() {this.playerNum++;}
    public void decrementPlayerNum() {this.playerNum--;}

    public long getPlayer2Status() {return player2Status;}
    public void setPlayer2Status(long status) {this.player2Status = status;}

    public boolean isGameOn() {return gameOn;}
    public void setGameOn() {this.gameOn = true;}
    public void setGameOff() {this.gameOn = false;}

}
