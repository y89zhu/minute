package koala.minute;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class GameRoomLiarGen extends AppCompatActivity {
    Firebase firebaseRef;
    String UID;
    String displayName;
    GameRoomLiar gameRoomLiar;
    GameLiar gameLiar;
    int myTurn;

    //list of text views
    TextView[] playerNameViews = new TextView[6];
    ImageView[] playerDiceViews = new ImageView[30];
    TextView[] bidMadeViews = new TextView[6];
    RadioButton[] facePickers = new RadioButton[6];
    String waiting = "waiting...";
    // 我的想法是：游戏本身的两个list保存所有玩家和所有玩家ROLL出来的骰子
    // 玩家列表也不用说谁是host，就按照进入的顺序update playerList然后Listen这个，然后用ListView来展现玩家列表
    // 然后每个玩家每次叫完之后，把叫的这个record传上去server，然后也是每个玩家maintain这个List
    // 如果是第一个创建的玩家就直接roll六个数字放进去string然后传上去这样，后面来的每个人是先把上面已有的东西读下来，再roll自己的六个
    // 意思就是在按下去START之前，每个人的dice就都跟着playerlist一起update，已经都分配好了
    // 游戏流程就是让每个玩家这样往下叫，直到有人喊开，然后把List-record里面的compute一遍发布结果
    // 感觉就是这样？？你看看怎么样



    ValueEventListener roomListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot snapshot) {
            // the first player to be the game holder
            //check the changes
            System.out.println("listen room " + snapshot);


            int playerNum = Integer.parseInt(snapshot.child("playerNum").getValue(String.class));
            int oldPlayerNum = Integer.parseInt(gameRoomLiar.getPlayerNum());
            int oldmyTurn = myTurn;
            boolean gameOn = snapshot.child("gameOn").getValue(boolean.class);
            String roomid = gameRoomLiar.getRoomID();
            TextView roomid_dis = (TextView)findViewById(R.id.roomID);
            roomid_dis.setText(roomid);
            TextView mybid_T = (TextView) findViewById(R.id.myBid_title);
            TextView mybid = (TextView) findViewById(R.id.myBid);
            System.out.println("old room " + gameRoomLiar.isGameOn());
            String oldreadyNum = gameRoomLiar.getReadyNum();


            //change this later
            // ((((((((((
            GameRoomLiar newInfo = new GameRoomLiar();
            newInfo.update(snapshot);

            for (Map.Entry<String, String> entry : newInfo.getUserInfo().entrySet())     //loop through the display to update myturn
            {
                int number = Integer.parseInt(entry.getValue().substring(0, 1));
                if(entry.getKey().equals(UID)) {myTurn = number;}

            }

            Button start = (Button) findViewById(R.id.start);
            Button ready = (Button) findViewById(R.id.ready);
            Button unready = (Button) findViewById(R.id.unready);
            System.out.println("old my turn " + oldmyTurn + " new " + myTurn );
            if(oldmyTurn != myTurn) {
                if(myTurn != 1) {
                    ready.setVisibility(View.VISIBLE);
                    start.setVisibility(View.INVISIBLE);
                    unready.setVisibility(View.INVISIBLE);

                } else {
                    start.setVisibility(View.VISIBLE) ;
                    ready.setVisibility(View.INVISIBLE);
                    unready.setVisibility(View.INVISIBLE);
                }
            }

            //))))))))))))))

            if(gameRoomLiar.isGameOn() && (!gameOn)) {            //if on -> off
                Toast.makeText(getApplicationContext(), "Game Finished.", Toast.LENGTH_LONG).show();
                //stop the game listener
                //updat Ui to cloesed game

                //display all dices
                System.out.println("on to off, show views");
                for (int i = 0; i<playerNum*5;i++) {
                    playerDiceViews[i].setVisibility(View.VISIBLE);
                }
                //buttons

                String restart = "restart";
                start.setText(restart);

                if(myTurn != 1) {
                    ready.setVisibility(View.VISIBLE);
                    start.setVisibility(View.INVISIBLE);
                    unready.setVisibility(View.INVISIBLE);

                } else {
                    start.setVisibility(View.VISIBLE) ;
                    ready.setVisibility(View.INVISIBLE);
                    unready.setVisibility(View.INVISIBLE);
                }

                //Button challengeb = (Button) findViewById(R.id.challenge);
                //challengeb.setVisibility(View.INVISIBLE);

                mybid.setVisibility(View.INVISIBLE);
                mybid_T.setVisibility(View.INVISIBLE);
                RelativeLayout normal_layout = (RelativeLayout)findViewById(R.id.normal_layout);
                RelativeLayout bid_layout = (RelativeLayout)findViewById(R.id.bid_layout);
                normal_layout.setVisibility(View.VISIBLE);
                bid_layout.setVisibility(View.GONE);

                //clear bid views
                for(int i=0;i<6;i++) {

                    bidMadeViews[i].setVisibility(View.GONE);
                }
                //stop the game listener
                firebaseRef.child("Games").child(gameRoomLiar.getRoomID()).removeEventListener(gameLisener);



            } else if ((!gameRoomLiar.isGameOn()) && gameOn) {    //off -> on
                Toast.makeText(getApplicationContext(), "Game started.", Toast.LENGTH_LONG).show();
                Map<String,String> userInfo = new HashMap<String,String>();
                userInfo = gameRoomLiar.getUserInfo();
                gameLiar = new GameLiar(gameRoomLiar.getRoomID(),userInfo);
                //show your dice
                //clear the previous game's result
                System.out.println("new game clear past views");
                for(int i=0;i<6;i++) {
                    for(int j=0;j<5;j++) {
                        playerDiceViews[i*5+j].setVisibility(View.GONE);

                    }
                    bidMadeViews[i].setVisibility(View.GONE);
                }
                System.out.println("new game show part view");
                for (int i = 0; i<6;i++) {

                    playerNameViews[i].setTextColor(Color.WHITE);
                }
                for (int i = 0; i<5;i++) {
                    playerDiceViews[(myTurn-1)*5+i].setVisibility(View.VISIBLE);

                }
                System.out.println("myturn is " + myTurn);
                //hide start button
                start.setVisibility(View.INVISIBLE);
                ready.setVisibility(View.INVISIBLE);
                unready.setVisibility(View.INVISIBLE);

                //challegeb.setVisibility(View.VISIBLE);
                mybid.setVisibility(View.VISIBLE);
                mybid_T.setVisibility(View.VISIBLE);

                if(myTurn == 1) {
                    gameLiar.upload();
                }
                //gameLiar.upload();



                firebaseRef.child("Games").child(gameRoomLiar.getRoomID()).addValueEventListener(gameLisener);
                //enable the game listener


            }
            if(playerNum == oldPlayerNum+1)  {                                 //new players coming in
                //should be 1 player at a time, since the changes on firebase should occur twice
                Toast.makeText(getApplicationContext(), "A new player has joined the room.", Toast.LENGTH_LONG).show();


            } else if (playerNum == oldPlayerNum-1) {
                Toast.makeText(getApplicationContext(), "A player has left the room.", Toast.LENGTH_LONG).show();
                //update display
                int leaveNum = Integer.parseInt(gameRoomLiar.getLeaveNum());
                //playerNameViews[leaveNum-1].setText(waiting);
                for(int i=0;i<6;i++) {
                    for(int j=0;j<5;j++) {
                        playerDiceViews[i+j].setVisibility(View.GONE);

                    }
                    playerNameViews[i].setText(waiting);
                    bidMadeViews[i].setVisibility(View.GONE);
                }
                gameRoomLiar.setGameOn(false);


            }


            //pull from server

            gameRoomLiar.update(snapshot);

            //update UI
            if(gameRoomLiar.getPlayerNum().equals(gameRoomLiar.getReadyNum()) && (!oldreadyNum.equals(gameRoomLiar.getPlayerNum()))) {
                Toast.makeText(getApplicationContext(), "Everyone is ready!.", Toast.LENGTH_LONG).show();
            } else if (!gameRoomLiar.getPlayerNum().equals(gameRoomLiar.getReadyNum()) && (oldreadyNum.equals(gameRoomLiar.getPlayerNum()))) {
                Toast.makeText(getApplicationContext(), "Someone is not ready again!.", Toast.LENGTH_LONG).show();
            }

            Map<String,String> userInfo = new HashMap<String,String>();
            userInfo = gameRoomLiar.getUserInfo();
            System.out.println(userInfo);
            int i = 0;

            System.out.println("updating name UI " + userInfo + "   " + snapshot);
            for (Map.Entry<String, String> entry : gameRoomLiar.getUserInfo().entrySet())     //loop through the display
            {
                int number = Integer.parseInt(entry.getValue().substring(0, 1));
                if(entry.getKey().equals(UID)) {myTurn = number;}
                String name = entry.getValue().substring(1);
                playerNameViews[number-1].setText(name);

            }


            //gameRoomLiar.upload();
            //firebaseRef.child("Rooms").child(gameRoomLiar.getRoomID()).addValueEventListener(roomListener);
/*
               // Create room and save it on server
               Firebase roomRef = new Firebase("https://koalaminute.firebaseio.com/Rooms");
               roomRef.push().setValue(this);*/

        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {
            System.out.println("createRoom listener failed");
        }
    };


    ValueEventListener gameLisener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot snapshot) {
            String oldWinner = gameLiar.getWinner();
            String oldBid = gameLiar.getLastBid();
            String oldTurn = gameLiar.getTurn();
            gameLiar.update(snapshot);
            System.out.println("game listen " + snapshot);
            String zhaiDisplay = " wild 1 unset";

            if(!gameLiar.getWinner().equals(oldWinner)) { //if the game is finished i.e. a new winner is set


                Toast.makeText(getApplicationContext(), "Challenge! The winner is " + gameLiar.getPlayerGameInfos().get(gameLiar.getWinner()).getDisplayName().substring(1), Toast.LENGTH_LONG).show();
                int winnerTurn = Integer.parseInt(gameLiar.getPlayerGameInfos().get(gameLiar.getWinner()).getDisplayName().substring(0,1));
                //winner's color get updated
                playerNameViews[winnerTurn-1].setTextColor(Color.BLUE);
                //the game room alrady set to false in server
                return;

            }

            //general UI update
            RelativeLayout normal_layout = (RelativeLayout)findViewById(R.id.normal_layout);
            RelativeLayout bid_layout = (RelativeLayout)findViewById(R.id.bid_layout);
            if(Integer.toString(myTurn).equals(gameLiar.getTurn())  )  {
                //hide the layout when it is my turn
                if(!oldTurn.equals(gameLiar.getTurn())) {
                    Toast.makeText(getApplicationContext(), "Now its your turn", Toast.LENGTH_LONG).show();
                }

                normal_layout.setVisibility(View.GONE);
                bid_layout.setVisibility(View.VISIBLE);
            } else {
                normal_layout.setVisibility(View.VISIBLE);
                bid_layout.setVisibility(View.GONE);
            }


            int i = 0;
            for (Map.Entry<String, PlayerGameInfo> entry : gameLiar.getPlayerGameInfos().entrySet())     //loop through the playergameinfo
            {
                int turn = Integer.parseInt(entry.getValue().getDisplayName().substring(0, 1));
                if(entry.getKey().equals(UID)) {
                    myTurn = turn;
                    //show the user's own bid
                    //construct the message
                    String myBid = entry.getValue().getBid();
                    String myBidM;
                    if(myBid.equals("0 0 0")) {
                        myBidM = "Not Bid Yet";
                    } else {
                        String bids[];
                        bids = myBid.split(" ");
                        int zhai = Integer.parseInt(bids[0]);
                        int face = Integer.parseInt(bids[1]);
                        int number = Integer.parseInt(bids[2]);
                        myBidM = "Face: " + face + " Amount: " + number;
                        if(zhai == 1) {
                            myBidM = myBidM + " with " + zhaiDisplay;
                        } else {
                            myBidM = myBidM + " without " + zhaiDisplay;
                        }

                    }
                    TextView myBid_display = (TextView) findViewById(R.id.myBid);
                    myBid_display.setText(myBidM);



                }
                String name = entry.getValue().getDisplayName().substring(1);
                //above is the same is the room listener

                //update all the dices display, the roomlisener controls wheather to display it or not
                String dice = entry.getValue().getDice();
                int dices[] = new int[5];
                for(int j=0;j<5;j++) {
                    dices[j] = Integer.parseInt(dice.substring(j,j+1));
                }
                displayDice(turn,dices);

                i++;
            }
            NumberPicker faceNumber = (NumberPicker) findViewById(R.id.faceNumber);
            int playerNum = Integer.parseInt(gameRoomLiar.getPlayerNum());
            faceNumber.setMinValue(playerNum);
            faceNumber.setMaxValue(20);
            //handling bid update
            //get all the last bid and construct mssage string
            String bids[] = new String[3];
            bids = gameLiar.getLastBid().split(" ");
            int zhai = Integer.parseInt(bids[0]);
            int face = Integer.parseInt(bids[1]);
            int number = Integer.parseInt(bids[2]);
            String bidInfo;
            bidInfo = "Face: " + face + " Amount: " + number;
            if(zhai == 1) {
                bidInfo = bidInfo + "with " + zhaiDisplay;
            } else {
                bidInfo = bidInfo + "without " + zhaiDisplay;
            }
            //if no bid yet
            if(gameLiar.getLastBid().equals("0 0 0")) {
                bidInfo = "Not Bid Yet";
            }
            //hide and show the views
            int currentTurn = Integer.parseInt(gameLiar.getTurn());
            int lastTurn = currentTurn-1;
            if (lastTurn < 1) {lastTurn = playerNum + lastTurn;}
            int turnBeforeLast = lastTurn -1;
            if (turnBeforeLast < 1) {turnBeforeLast = playerNum + turnBeforeLast;}
            if(myTurn != lastTurn) { //otherwise the textview and dice view will show together
                bidMadeViews[lastTurn-1].setText(bidInfo);
                bidMadeViews[lastTurn-1].setVisibility(View.VISIBLE);
            }
            //if condition to prevent 1 player starting game to crash
            if(playerNum > 1) {bidMadeViews[turnBeforeLast-1].setVisibility(View.GONE);}




        }



        @Override
        public void onCancelled(FirebaseError firebaseError) {
            System.out.println("game listener cancelled");
        }
    };
    void displayDice(int playerTurn,int[] dices) {
        for(int j=0;j<5;j++) {

            if(dices[j] == 1) {
                playerDiceViews[(playerTurn-1)*5+j].setImageResource(R.drawable.dice1x);
            } else if (dices[j] == 2) {
                playerDiceViews[(playerTurn-1)*5+j].setImageResource(R.drawable.dice2);
            } else if(dices[j] == 3) {
                playerDiceViews[(playerTurn-1)*5+j].setImageResource(R.drawable.dice3);
            } else if(dices[j] == 4) {
                playerDiceViews[(playerTurn-1)*5+j].setImageResource(R.drawable.dice4);
            } else if(dices[j] == 5) {
                playerDiceViews[(playerTurn-1)*5+j].setImageResource(R.drawable.dice5);
            } else {
                playerDiceViews[(playerTurn-1)*5+j].setImageResource(R.drawable.dice6);
            }

        }
    }

    public void leave(View view) {
        //disable the listeners
        firebaseRef.child("Rooms").child(gameRoomLiar.getRoomID()).removeEventListener(roomListener);

        if(gameRoomLiar.isGameOn()) {
            firebaseRef.child("Games").child(gameRoomLiar.getRoomID()).removeEventListener(gameLisener);
        }
        if(gameRoomLiar.getPlayerNum().equals("1")) {
            firebaseRef.child("Rooms").child(gameRoomLiar.getRoomID()).setValue(null);
            if(gameRoomLiar.isGameOn()) {
                firebaseRef.child("Games").child(gameRoomLiar.getRoomID()).setValue(null);
            }
            finish();
            return;
        }
        gameRoomLiar.removePlayer(UID);gameRoomLiar.setGameOn(false);
        gameRoomLiar.setReadyNum("1"); //set to 0 to refresh ready
     //   gameLiar.removePlayer(UID);
        gameRoomLiar.upload();
        finish();
    }

    public void start(View view) {
        if(gameRoomLiar.getPlayerNum().equals(gameRoomLiar.getReadyNum())) {
            firebaseRef.child("Rooms").child(gameRoomLiar.getRoomID()).child("gameOn").setValue(true);
        } else {
            Toast.makeText(getApplicationContext(), "Other players are not ready yet", Toast.LENGTH_LONG).show();
        }

    }
//ready on click
    public void ready(View view) {
        int readyNum = Integer.parseInt(gameRoomLiar.getReadyNum()) ;
        readyNum++;
        gameRoomLiar.setReadyNum(Integer.toString(readyNum));
        gameRoomLiar.upload();
        Button ready = (Button) findViewById(R.id.ready);
        Button unready = (Button) findViewById(R.id.unready);
        ready.setVisibility(View.INVISIBLE);
        unready.setVisibility(View.VISIBLE);
    }
    //unready on click
    public void unready(View view) {
        int readyNum = Integer.parseInt(gameRoomLiar.getReadyNum()) ;
        readyNum--;
        gameRoomLiar.setReadyNum(Integer.toString(readyNum));
        gameRoomLiar.upload();
        gameRoomLiar.upload();
        Button ready = (Button) findViewById(R.id.ready);
        Button unready = (Button) findViewById(R.id.unready);
        ready.setVisibility(View.VISIBLE);
        unready.setVisibility(View.INVISIBLE);
    }

    //challnge click
    public void challenge(View view) {

       //get the bid information
        String bids[] = new String[3];
        bids = gameLiar.getLastBid().split(" ");
        if (gameLiar.getLastBid().equals("0 0 0")) {
            Toast.makeText(getApplicationContext(), "You must wait until the player played", Toast.LENGTH_LONG).show();
            return;
        }
        int zhai = Integer.parseInt(bids[0]);
        int face = Integer.parseInt(bids[1]);
        int number = Integer.parseInt(bids[2]);
        if(face == 1) {zhai = 1;}
        int result = 0;
        int result_without = 0;

        //count number of dices really
        for (Map.Entry<String, PlayerGameInfo> entry : gameLiar.getPlayerGameInfos().entrySet())     //loop through the playergameinfo to count the faces
        {
            int[] dices = new int[5];
            String dice_string = entry.getValue().getDice();
            boolean all_match = true;
            for(int j = 0;j<5;j++) {
                dices[j] = Integer.parseInt(dice_string.substring(j,j+1));
                if(dices[j] == 1) {result++;}
                else if (dices[j] == face) {result++;result_without++;}
                else {all_match = false;}
            }
            if (all_match) {result++;}


        }
        //compare the number of faces
        String tempwinner = gameLiar.getWinner();
        System.out.println(gameLiar.getLastBid() + "game result is " + result + " with zhai and " + result_without);
        if(zhai == 1) {
            if(result_without > number) {
                gameLiar.winner = gameLiar.getLastPlayer();
            } else {
                gameLiar.winner = UID;
            }
        } else if(zhai ==0) {
            if(result > number) {
                gameLiar.winner = gameLiar.getLastPlayer();
            } else {
                gameLiar.winner = UID;
            }
        }
        gameRoomLiar.setGameOn(false);
        gameLiar.upload();
        gameRoomLiar.setReadyNum("1");
        gameRoomLiar.upload();

        gameRoomLiar.setGameOn(true);
        gameLiar.setWinner(tempwinner);

    }

    //bid click
    public void bid(View view) {
        //get face
        RadioGroup facePicker = (RadioGroup) findViewById(R.id.facePicker);
        int checkedFace = facePicker.getCheckedRadioButtonId();
        RadioButton checkButoon = (RadioButton) findViewById(checkedFace);
        int chosenface_int = facePicker.indexOfChild(checkButoon)+1;
        if(chosenface_int == 0) {
            return;
        }
        String chosenFace = Integer.toString(chosenface_int);
        // get number
        NumberPicker numberPicker = (NumberPicker) findViewById(R.id.faceNumber);
        int facenumber = numberPicker.getValue();
        //get checkbox
        CheckBox zhai = (CheckBox) findViewById(R.id.zhai);
        boolean checked = zhai.isChecked();
        int checkZhai = 0;
        if(checked || (chosenface_int == 1)) {checkZhai = 1;}

        //get the last bid info
        String bids[] = new String[3];
        bids = gameLiar.getLastBid().split(" ");
        int lastzhai = Integer.parseInt(bids[0]);
        int lastface = Integer.parseInt(bids[1]);
        int lastnumber = Integer.parseInt(bids[2]);
        //now check if the bid is valid or not
        if(lastface == 1) {lastzhai = 1;}
        int bidValue = (checkZhai+1)*facenumber;
        int last_bidValue = (lastzhai+1) * lastnumber;

        int currentFace = Integer.parseInt(chosenFace);
        System.out.println("biding , the cureent value " +bidValue + " last "+last_bidValue );
        if((bidValue<last_bidValue) || (bidValue ==last_bidValue && currentFace <= lastface)) {
            //the current bid must be larger
            Toast.makeText(getApplicationContext(), "You must mak a larger bid", Toast.LENGTH_LONG).show();
            return;
        }

        //increment turn eg. (3+1)/3 = 1
        int turn = (Integer.parseInt(gameLiar.getTurn())+1);
        int playerNum = Integer.parseInt(gameRoomLiar.getPlayerNum());
        if(turn  > playerNum) {turn = turn - playerNum;}
        System.out.println("turn" + turn);
        gameLiar.setTurn(Integer.toString(turn));
        //update
        String bid = checkZhai + " " + chosenFace + " " + facenumber;
        gameLiar.updateBid(UID, bid);
        gameLiar.setLastPlayer(UID);
        gameLiar.upload();

    }



    public class GameLiar {
        // this class is used to store user dices
        private Map<String,PlayerGameInfo> playerGameInfos;
        private String roomID;
        private String winner;
        private String lastBid;
        private String turn;
        private String lastPlayer;


        public GameLiar() {}

        public GameLiar(String roomId, Map<String,String> userInfo) {
            this.winner = "0";
            this.roomID = roomId;
            this.lastBid = "0 0 0";
            this.turn = "1";
            playerGameInfos = new HashMap<String,PlayerGameInfo>();
            for (Map.Entry<String, String> entry : userInfo.entrySet())     //loop playerGameInfos
            {
                PlayerGameInfo player = new PlayerGameInfo(entry.getValue());
                playerGameInfos.put(entry.getKey(),player);

            }


        }
        public Map<String,PlayerGameInfo> getPlayerGameInfos() {
            return  playerGameInfos;
        }
        public String getRoomID() {
            return  roomID;
        }
        public String getTurn() {return  turn;}
        public String getLastBid(){return lastBid;}
        public String getWinner() {
            return  winner;
        }
        public String getLastPlayer() {return lastPlayer;}
        public void setTurn(String turn) {this.turn = turn;}
        public void setWinner(String winner) {
            this.winner = winner;
        }
        public void setLastPlayer(String lastPlayer) {
            this.lastPlayer = lastPlayer;
        }
        public void setLastBid(String lastBid) {this.lastBid=lastBid;}
        //update from a snapshot
        public void update(DataSnapshot snapshot) {
            winner = snapshot.child("winner").getValue(String.class);
            roomID = snapshot.child("roomID").getValue(String.class);
            turn = snapshot.child("turn").getValue(String.class);
            lastBid = snapshot.child("lastBid").getValue(String.class);
            lastPlayer  = snapshot.child("lastPlayer").getValue(String.class);
            for (DataSnapshot userSnapshot: snapshot.child("playerGameInfos").getChildren()) {
                PlayerGameInfo singleInfo = new PlayerGameInfo();
                singleInfo.setBid(userSnapshot.child("bid").getValue(String.class));
                singleInfo.setDice(userSnapshot.child("dice").getValue(String.class));
                singleInfo.setDisplayName(userSnapshot.child("displayName").getValue(String.class));
                singleInfo.setChallenge(userSnapshot.child("challenge").getValue(boolean.class));

                playerGameInfos.put(userSnapshot.getKey(), singleInfo);
            }

        }

        //update to the server
        public void upload() {
            Firebase gameRef = new Firebase("https://koalaminute.firebaseio.com/Games/"+roomID+"/");
            gameRef.child("winner").setValue(winner);
            gameRef.child("roomID").setValue(roomID);
            gameRef.child("turn").setValue(turn);
            gameRef.child("lastBid").setValue(lastBid);
            gameRef.child("lastPlayer").setValue(lastPlayer);
            gameRef.child("playerGameInfos").setValue(1);
            for (Map.Entry<String, PlayerGameInfo> entry : playerGameInfos.entrySet())
            {

                gameRef.child("playerGameInfos").child(entry.getKey()).setValue(entry.getValue());
            }

        }

        public void updateBid(String id, String bid) {
            //update the last bid and the bid that belongs to the player
            lastBid = bid;
            for (Map.Entry<String, PlayerGameInfo> entry : playerGameInfos.entrySet())
            {
                System.out.println("made bid");
                if(entry.getKey().equals(id)) {entry.getValue().setBid(bid);}
            }

        }



    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //hide the title bar
       // this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_game_room_liar_gen);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
    //initialize some global variables
        playerNameViews[0] = (TextView)findViewById(R.id.player1Name);
        playerNameViews[1] = (TextView)findViewById(R.id.player2Name);
        playerNameViews[2] = (TextView)findViewById(R.id.player3Name);
        playerNameViews[3] = (TextView)findViewById(R.id.player4Name);
        playerNameViews[4] = (TextView)findViewById(R.id.player5Name);
        playerNameViews[5] = (TextView)findViewById(R.id.player6Name);
        //harding coding the entire list of the dices
        String dice = "player1dice3";


        firebaseRef = new Firebase("https://koalaminute.firebaseio.com/");
        AuthData authData = firebaseRef.getAuth();
        if (authData != null) {
            UID = authData.getUid();
            System.out.println("UID: " + UID);
        } else {
            System.err.println("No authData in GameRoomLiarGen");
        }
        //get room id from passing parameter and dipslay name from sharepereference
        final Bundle roomid = getIntent().getExtras();
        System.out.println(firebaseRef.child("RoomIDcounter"));
        SharedPreferences sharedPreferences = this.getSharedPreferences("koala.minute", Context.MODE_PRIVATE);
        displayName = sharedPreferences.getString("displayName","");

        //initializing
        // If the player is creating a new room
        if (roomid == null) {
            gameRoomLiar = new GameRoomLiar(UID,displayName);
            //listen once to setup the roomid
            ValueEventListener createRoom = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    String RoomID;
                    // Retrieve RoomIDcounter from server and increment it by 1
                    String idString;
                    idString = snapshot.getValue(String.class);
                    String myID = Integer.toString(Integer.parseInt(idString) + 1);
                    // Save the updated RoomIDcounter on server
                    firebaseRef.child("RoomIDcounter").setValue(myID);
                    RoomID = myID;
                    System.out.println("set " + RoomID);
                    gameRoomLiar.setRoomID(RoomID);


                    gameRoomLiar.upload();
                     firebaseRef.child("Rooms").child(gameRoomLiar.getRoomID()).addValueEventListener(roomListener);
/*
               // Create room and save it on server
               Firebase roomRef = new Firebase("https://koalaminute.firebaseio.com/Rooms");
               roomRef.push().setValue(this);*/

                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {
                    System.out.println("createRoom listener failed");
                }
            };
            firebaseRef.child("RoomIDcounter").addListenerForSingleValueEvent(createRoom);
        //    firebaseRef.removeEventListener(createRoom);


        } else {
            gameRoomLiar = new GameRoomLiar();
            String id = roomid.getString("ID");
            System.out.println("join new room " + id);
            firebaseRef.child("Rooms").child(id);

            ValueEventListener joinRoom = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    if (snapshot == null) {
                        Toast.makeText(getApplicationContext(), "Invalid Game Room ID.", Toast.LENGTH_LONG).show();
                        finish();
                    }
                    gameRoomLiar.update(snapshot);
                    //update playernum and player
                   // int playerNum = Integer.parseInt(snapshot.child("playerNum").getValue(String.class));
                    //playerNum++;
                    gameRoomLiar.addPlayer(UID, displayName);
                    gameRoomLiar.upload();
                    firebaseRef.child("Rooms").child(gameRoomLiar.getRoomID()).addValueEventListener(roomListener);
                   ;
                }


                @Override
                public void onCancelled(FirebaseError firebaseError) {
                    System.out.println("User/UserID create failed");
                }
            };
            firebaseRef.child("Rooms").child(id).addListenerForSingleValueEvent(joinRoom);
            firebaseRef.removeEventListener(joinRoom);



        }

        playerDiceViews[0] = (ImageView)findViewById(R.id.player1dice1);
        playerDiceViews[1] = (ImageView)findViewById(R.id.player1dice2);
        playerDiceViews[2] = (ImageView)findViewById(R.id.player1dice3);
        playerDiceViews[3] = (ImageView)findViewById(R.id.player1dice4);
        playerDiceViews[4] = (ImageView)findViewById(R.id.player1dice5);
        playerDiceViews[5] = (ImageView)findViewById(R.id.player2dice1);
        playerDiceViews[6] = (ImageView)findViewById(R.id.player2dice2);
        playerDiceViews[7] = (ImageView)findViewById(R.id.player2dice3);
        playerDiceViews[8] = (ImageView)findViewById(R.id.player2dice4);
        playerDiceViews[9] = (ImageView)findViewById(R.id.player2dice5);
        playerDiceViews[10] = (ImageView)findViewById(R.id.player3dice1);
        playerDiceViews[11] = (ImageView)findViewById(R.id.player3dice2);
        playerDiceViews[12] = (ImageView)findViewById(R.id.player3dice3);
        playerDiceViews[13] = (ImageView)findViewById(R.id.player3dice4);
        playerDiceViews[14] = (ImageView)findViewById(R.id.player3dice5);
        playerDiceViews[15] = (ImageView)findViewById(R.id.player4dice1);
        playerDiceViews[16] = (ImageView)findViewById(R.id.player4dice2);
        playerDiceViews[17] = (ImageView)findViewById(R.id.player4dice3);
        playerDiceViews[18] = (ImageView)findViewById(R.id.player4dice4);
        playerDiceViews[19] = (ImageView)findViewById(R.id.player4dice5);
        playerDiceViews[20] = (ImageView)findViewById(R.id.player5dice1);
        playerDiceViews[21] = (ImageView)findViewById(R.id.player5dice2);
        playerDiceViews[22] = (ImageView)findViewById(R.id.player5dice3);
        playerDiceViews[23] = (ImageView)findViewById(R.id.player5dice4);
        playerDiceViews[24] = (ImageView)findViewById(R.id.player5dice5);
        playerDiceViews[25] = (ImageView)findViewById(R.id.player6dice1);
        playerDiceViews[26] = (ImageView)findViewById(R.id.player6dice2);
        playerDiceViews[27] = (ImageView)findViewById(R.id.player6dice3);
        playerDiceViews[28] = (ImageView)findViewById(R.id.player6dice4);
        playerDiceViews[29] = (ImageView)findViewById(R.id.player6dice5);
        bidMadeViews[0] = (TextView) findViewById(R.id.bid1);
        bidMadeViews[1] = (TextView) findViewById(R.id.bid2);
        bidMadeViews[2] = (TextView) findViewById(R.id.bid3);
        bidMadeViews[3] = (TextView) findViewById(R.id.bid4);
        bidMadeViews[4] = (TextView) findViewById(R.id.bid5);
        bidMadeViews[5] = (TextView) findViewById(R.id.bid6);
        facePickers[0] = (RadioButton) findViewById(R.id.radiodice1);
        facePickers[1] = (RadioButton) findViewById(R.id.radiodice2);
        facePickers[2] = (RadioButton) findViewById(R.id.radiodice3);
        facePickers[3] = (RadioButton) findViewById(R.id.radiodice4);
        facePickers[4] = (RadioButton) findViewById(R.id.radiodice5);
        facePickers[5] = (RadioButton) findViewById(R.id.radiodice6);






    }

}
