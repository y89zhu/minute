package koala.minute;

import java.util.Random;

/**
 * Created by user on 2016/3/7.
 */
public class PlayerGameInfo {
    private String displayName;
    private String dice;
    private String bid;
    private boolean challenge;
    PlayerGameInfo() {}
    PlayerGameInfo(String displayName) {
        this.displayName = displayName;
        //generate dice
        String dice = new String();
        for (int i=0;i<5;i++) {
            Random randomDice = new Random();
            int onedice = 1+randomDice.nextInt(6);
            dice = dice + Integer.toString(onedice);
        }

        this.dice = dice;
        this.bid = "0 0 0";
        challenge = false;
    }

    public void setDisplayName (String displayName) {
        this.displayName = displayName;
    }
    public void setDice (String dice) {
        this.dice = dice;
    }
    public void  setBid (String bid) {
        this.bid = bid;
    }
    public void setChallenge(boolean challenge) {this.challenge = challenge;}
    public String getDisplayName () {
        return displayName;
    }
    public String getDice() {
        return dice;
    }
    public String getBid() {
        return  bid;
    }
    public boolean isChallenge() {return challenge;}

}
