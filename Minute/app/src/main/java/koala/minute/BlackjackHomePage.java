package koala.minute;
// background image: https://pixabay.com/en/wordpress-background-web-design-581849/
// edited for non-commercial use
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;

public class BlackjackHomePage extends AppCompatActivity implements DialogFragmentDisplayName.NoticeDialogListener{
    private Animation animationtrans;
    Firebase firebaseRef;
    TextView userNameField;
    String UID;
    SharedPreferences sharedPreferences;

/*
    ValueEventListener createRoom = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot snapshot) {
            if (snapshot == null) {
                Toast.makeText(getApplicationContext(), "Invalid Game Room ID.", Toast.LENGTH_LONG).show();
                finish();
            }
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {
            System.out.println("join room cancelld");
        }
    };*/






    public void startBlackjack(View view){
        view.startAnimation(animationtrans);
        Intent i = new Intent(getApplicationContext(), Blackjack.class);
        startActivity(i);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blackjack_home_page);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        animationtrans = new AnimationUtils().loadAnimation(this,R.anim.anim_translate);

        firebaseRef = new Firebase("https://koalaminute.firebaseio.com/");
        userNameField = (TextView) findViewById(R.id.displayName);
        sharedPreferences = this.getSharedPreferences("koala.minute", Context.MODE_PRIVATE);

        AuthData authData = firebaseRef.getAuth();
        if (authData != null) {
            UID = authData.getUid();
            System.out.println("UID: " + UID);
        } else {
            System.out.println("No authData in HomePage");
        }
        if(UID == null) {
            System.out.println("home page no auth, wh");
            firebaseRef.unauth();
            Intent intent = new Intent(this, LoginSignup.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return;
        }

        // Retrieve user's display name

        //firebaseRef.child("Users").child(UID).child("displayName").removeEventListener(listenName);
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {

    }
}