package koala.minute;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;

public class GameRoomBattle {
    private String type;
    private String roomID;
    private String gameHolderID;
    private String player1;
    private String player1ID;
    private String player2;
    private String player2ID;
    private String player3;
    private String player3ID;
    private String player4;
    private String player4ID;
    private long playerNum;
    private long player1Status;
    private long player2Status;
    private long player3Status;
    private long player4Status;
    public boolean gameOn;

    public GameRoomBattle(){}

    public GameRoomBattle(String gameHolderID) {
        this.type = "battle";
        this.gameHolderID = gameHolderID;
        this.player1 = "";
        this.player2 = "";
        this.player3 = "";
        this.player4 = "";
        this.player1ID = gameHolderID;
        this.player2ID = "";
        this.player3ID = "";
        this.player4ID = "";
        this.playerNum = 0;
        // status: 0:not available 1:not ready 2:ready
        this.player1Status = 0;
        this.player2Status = 0;
        this.player3Status = 0;
        this.player4Status = 0;
        this.gameOn = false;
    }

    public String getRoomID() {return this.roomID;}
    public void setRoomID(String roomID) {this.roomID = roomID;}

    public void setPlayer1(String player1) {this.player1 = player1;}
    public void setPlayer2(String player2) {this.player2 = player2;}
    public String getPlayer1() {return player1;}
    public String getPlayer2() {return player2;}
    public String getPlayer3() {return player3;}
    public String getPlayer4() {return player4;}

    public long getPlayer1Status() {return player1Status;}
    public long getPlayer2Status() {return player2Status;}
    public long getPlayer3Status() {return player3Status;}
    public long getPlayer4Status() {return player4Status;}

    public String getPlayer1ID() {return player1ID;}
    public String getPlayer2ID() {return player2ID;}

    public String getGameHolderID() {return gameHolderID;}
    public void setGameHolderID(String gameHolderID) {this.gameHolderID= gameHolderID;}

    public long getPlayerNum() {return playerNum;}
    public void setPlayerNum(long playerNum) {this.playerNum = playerNum;}
    public void incrementPlayerNum() {this.playerNum++;}
    public void decrementPlayerNum() {this.playerNum--;}

    public boolean isGameOn() {return gameOn;}
    public void setGameOn() {this.gameOn = true;}
    public void setGameOff() {this.gameOn = false;}

    // Return the next available position for the coming player
    // Return 0 if the room is full
    // status: 0:not available 1:not ready 2:ready
    public int nextAvailable() {
        if (player2Status == 0) {
            return 2;
        } else if (player3Status == 0) {
            return 3;
        } else if (player4Status == 0) {
            return 4;
        } else {
            return 0;
        }
    }

    // Return true if the game is allow to start
    public boolean ableToStart() {
        int check = 0;
        if (player2Status == 2) {check++;}
        if (player3Status == 2) {check++;}
        if (player4Status == 2) {check++;}
        return check >= 1;
    }

    // Set player display name to given which player and status code
    public void setPlayerName(int playerNumber, String name) {
        if (playerNumber == 1) {
            player1 = name;
        } else if (playerNumber == 2) {
            player2 = name;
        } else if (playerNumber == 3) {
            player3 = name;
        } else if (playerNumber == 4) {
            player4 = name;
        }
    }

    public int checkPlayerByName(String playerName) {
        if (playerName.equals(player1)) {
            return 1;
        } else if (playerName.equals(player2)) {
            return 2;
        } else if (playerName.equals(player3)) {
            return 3;
        } else if (playerName.equals(player4)) {
            return 4;
        } else {
            return 0;
        }
    }

    // Set player UID to given which player and status code
    public void setPlayerID(int playerNumber, String UID) {
        if (playerNumber == 1) {
            player1ID = UID;
        } else if (playerNumber == 2) {
            player2ID = UID;
        } else if (playerNumber == 3) {
            player3ID = UID;
        } else if (playerNumber == 4) {
            player4ID = UID;
        }
    }

    // Set player status according to given which player and status code
    public void setPlayerStatus(int playerNumber, long status) {
        if (playerNumber == 1) {
            player1Status = status;
        } else if (playerNumber == 2) {
            player2Status = status;
        } else if (playerNumber == 3) {
            player3Status = status;
        } else if (playerNumber == 4) {
            player4Status = status;
        }
    }

    public void update(DataSnapshot snapshot) {
        // no update on type
        gameHolderID = snapshot.child("gameHolderID").getValue(String.class);
        roomID = snapshot.child("roomID").getValue(String.class);
        player1 = snapshot.child("player1").getValue(String.class);
        player2 = snapshot.child("player2").getValue(String.class);
        player3 = snapshot.child("player3").getValue(String.class);
        player4 = snapshot.child("player4").getValue(String.class);
        player1ID = snapshot.child("player1ID").getValue(String.class);
        player2ID = snapshot.child("player2ID").getValue(String.class);
        player3ID = snapshot.child("player3ID").getValue(String.class);
        player4ID = snapshot.child("player4ID").getValue(String.class);
        playerNum = snapshot.child("playerNum").getValue(long.class);
        player1Status = snapshot.child("player1Status").getValue(long.class);
        player2Status = snapshot.child("player2Status").getValue(long.class);
        player3Status = snapshot.child("player3Status").getValue(long.class);
        player4Status = snapshot.child("player4Status").getValue(long.class);
        gameOn = snapshot.child("gameOn").getValue(boolean.class);
    }

    public void upload() {
        // no upload on type
        Firebase roomRef = new Firebase("https://koalaminute.firebaseio.com/Rooms/"+roomID+"/");
        roomRef.child("roomID").setValue(roomID);
        roomRef.child("gameHolderID").setValue(gameHolderID);
        roomRef.child("player1").setValue(player1);
        roomRef.child("player2").setValue(player2);
        roomRef.child("player3").setValue(player3);
        roomRef.child("player4").setValue(player4);
        roomRef.child("player1ID").setValue(player1ID);
        roomRef.child("player2ID").setValue(player2ID);
        roomRef.child("player3ID").setValue(player3ID);
        roomRef.child("player4ID").setValue(player4ID);
        roomRef.child("playerNum").setValue(playerNum);
        roomRef.child("player1Status").setValue(player1Status);
        roomRef.child("player2Status").setValue(player2Status);
        roomRef.child("player3Status").setValue(player3Status);
        roomRef.child("player4Status").setValue(player4Status);
        roomRef.child("gameOn").setValue(gameOn);
    }

    public void uploadPlayerStatus(int playerNumber, long status) {
        Firebase roomRef = new Firebase("https://koalaminute.firebaseio.com/Rooms/"+roomID+"/");
        if (playerNumber == 1) {
            roomRef.child("player1Status").setValue(status);
        } else if (playerNumber == 2) {
            roomRef.child("player2Status").setValue(status);
        } else if (playerNumber == 3) {
            roomRef.child("player3Status").setValue(status);
        } else if (playerNumber == 4) {
            roomRef.child("player4Status").setValue(status);
        }
    }
}
