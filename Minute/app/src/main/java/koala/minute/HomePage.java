package koala.minute;
// background image: https://pixabay.com/en/wordpress-background-web-design-581849/
// edited for non-commercial use
import android.app.ActivityOptions;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.transition.Explode;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

public class HomePage extends AppCompatActivity implements DialogFragmentDisplayName.NoticeDialogListener{
   // private ProgressDialog progressDialog;

    private Animation animationtrans;
    Firebase firebaseRef;
    TextView userNameField;
    String UID;
    SharedPreferences sharedPreferences;
    ValueEventListener listenName = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot snapshot) {
            String name;
            name = snapshot.getValue(String.class);
            TextView username = (TextView)findViewById(R.id.displayName);
            username.setText(name);
            sharedPreferences.edit().putString("displayName", name).apply();
        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {
            System.out.println("Homepage - listenName failed");
        }
    };

    public void changeDisplayName (View view) {
        DialogFragment dialog = new DialogFragmentDisplayName();
        dialog.show(getFragmentManager(), "DisplayNameFragment");
        //firebaseRef.child("Users").child(UID).child("displayName").addListenerForSingleValueEvent(listenName);
    }


    public void logout(View view) {
        view.startAnimation(animationtrans);
        firebaseRef.unauth();
        sharedPreferences.edit().putString("username", null).apply();
        sharedPreferences.edit().putString("password", null).apply();
        Intent intent = new Intent(this, LoginSignup.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void blackjack(View view){
        // Enable explode transitions if system version is at least API21
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setExitTransition(new Explode());
            Intent intent = new Intent(this, BlackjackHomePage.class);
            startActivity(intent,
                    ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else {
            view.startAnimation(animationtrans);
            Intent i = new Intent(getApplicationContext(), BlackjackHomePage.class);
            startActivity(i);
            overridePendingTransition(R.anim.activity_anim, R.anim.activity_finish);
        }
    }

    public void GuessNumber(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setExitTransition(new Explode());
            Intent intent = new Intent(this, GuessNumberHomePage.class);
            startActivity(intent,
                    ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else {
            view.startAnimation(animationtrans);
            Intent intent = new Intent(this, GuessNumberHomePage.class);
            startActivity(intent);
        }
    }


    public void Battle_Game(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setExitTransition(new Explode());
            Intent intent = new Intent(this, BattleHomePage.class);
            startActivity(intent,
                    ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else {
            view.startAnimation(animationtrans);
            Intent intent = new Intent(this, BattleHomePage.class);
            startActivity(intent);
        }
    }

    public void join_Guess(View view) {
        EditText room_id = (EditText) findViewById(R.id.inputMax);
        final String parseId = room_id.getText().toString();
        final Intent intent = new Intent(this, GameRoomGen.class);

        // Pass the roomID to new activity

        ValueEventListener createRoom = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.getValue() == null) {
                    Toast.makeText(getApplicationContext(), "Invalid Game Room ID.", Toast.LENGTH_LONG).show();
                    return;
                }
                Bundle RoomID = new Bundle();
                RoomID.putString("ID", parseId);
                intent.putExtras(RoomID);
                startActivity(intent);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                System.out.println("join room cancelld");
            }
        };
        firebaseRef.child("Rooms").child(parseId).addListenerForSingleValueEvent(createRoom);
        firebaseRef.removeEventListener(createRoom);
    }

    public void createLiar(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setExitTransition(new Explode());
            Intent intent = new Intent(this, LiarHomePage.class);
            startActivity(intent,
                    ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else {
            view.startAnimation(animationtrans);
            Intent intent = new Intent(this, LiarHomePage.class);
            startActivity(intent);
        }
    }


    public void swapIntoGamesLayout(View view) {
        final RelativeLayout gamesLayout = (RelativeLayout) findViewById(R.id.gamesLayout);
        final RelativeLayout settingsLayout = (RelativeLayout) findViewById(R.id.settingsLayout);
        final ImageButton tab1Icon = (ImageButton) findViewById(R.id.tab1_icon);
        tab1Icon.setImageResource(R.drawable.ic_gamepad_white_24dp);
        final ImageButton tab2Icon = (ImageButton) findViewById(R.id.tab2_icon);
        tab2Icon.setImageResource(R.drawable.ic_settings_black_24dp);

        if (settingsLayout.getVisibility() == View.VISIBLE) {
            Animation slideIn = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_in_left);
            Animation slideOut = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_out_right);
            slideIn.setDuration(300);
            slideOut.setDuration(300);

            settingsLayout.startAnimation(slideOut);
            settingsLayout.setVisibility(View.GONE);
            gamesLayout.startAnimation(slideIn);
            gamesLayout.setVisibility(View.VISIBLE);
        }
    }


    public void swapIntoSettingsLayout(View view) {
        final RelativeLayout gamesLayout = (RelativeLayout) findViewById(R.id.gamesLayout);
        final RelativeLayout settingsLayout = (RelativeLayout) findViewById(R.id.settingsLayout);
        final ImageButton tab1Icon = (ImageButton) findViewById(R.id.tab1_icon);
        tab1Icon.setImageResource(R.drawable.ic_gamepad_black_24dp);
        final ImageButton tab2Icon = (ImageButton) findViewById(R.id.tab2_icon);
        tab2Icon.setImageResource(R.drawable.ic_settings_white_24dp);

        if (gamesLayout.getVisibility() == View.VISIBLE) {
            Animation slideIn = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_in_left);
            Animation slideOut = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.slide_out_right);
            slideIn.setDuration(300);
            slideOut.setDuration(300);

            gamesLayout.startAnimation(slideOut);
            gamesLayout.setVisibility(View.GONE);
            settingsLayout.startAnimation(slideIn);
            settingsLayout.setVisibility(View.VISIBLE);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //new LoadViewTask().execute();
        // Enable transitions if system version is at least API21
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        animationtrans = new AnimationUtils().loadAnimation(this,R.anim.anim_translate);

        firebaseRef = new Firebase("https://koalaminute.firebaseio.com/");

        userNameField = (TextView) findViewById(R.id.displayName);
        sharedPreferences = this.getSharedPreferences("koala.minute", Context.MODE_PRIVATE);

        AuthData authData = firebaseRef.getAuth();
        if (authData != null) {
            UID = authData.getUid();
            System.out.println("UID: " + UID);
        } else {
            System.out.println("No authData in HomePage");
        }
        if(UID == null) {
            System.out.println("home page no auth, wh");
            firebaseRef.unauth();
            Intent intent = new Intent(this, LoginSignup.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return;
        }

        // Retrieve user's display name
        firebaseRef.child("Users").child(UID).child("displayName").addValueEventListener(listenName);
        //firebaseRef.child("Users").child(UID).child("displayName").removeEventListener(listenName);

    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {

    }

    //
    //
    //
    //
    //
    //To use the AsyncTask, it must be subclassed

//    private class LoadViewTask extends android.os.AsyncTask<Void, Integer, Void>
//    {
//        //Before running code in separate thread
//        @Override
//        protected void onPreExecute()
//        {
//            progressDialog = ProgressDialog.show(HomePage.this,"Loading...",
//                    "Loading application View, please wait...", false, false);
//            //This dialog can't be canceled by pressing the back key
//            progressDialog.setCancelable(false);
//            //This dialog isn't indeterminate
//            progressDialog.setIndeterminate(false);
//            //The maximum number of items is 100
//            progressDialog.setMax(100);
//            //Set the current progress to zero
//            progressDialog.setProgress(0);
//            //Display the progress dialog
//            progressDialog.show();
//
//        }
//
//        //The code to be executed in a background thread.
//        @Override
//        protected Void doInBackground(Void... params)
//        {
//            /* This is just a code that delays the thread execution 4 times,
//             * during 850 milliseconds and updates the current progress. This
//             * is where the code that is going to be executed on a background
//             * thread must be placed.
//             */
//            try
//            {
//                //Get the current thread's token
//                synchronized (this)
//                {
//                    //Initialize an integer (that will act as a counter) to zero
//                    int counter = 0;
//                    //While the counter is smaller than four
//                    while(counter <= 2)
//                    {
//                        //Wait 850 milliseconds
//                        this.wait(200);
//                        //Increment the counter
//                        counter++;
//                        //Set the current progress.
//                        //This value is going to be passed to the onProgressUpdate() method.
//                        publishProgress(counter*90);
//                    }
//                }
//            }
//            catch (InterruptedException e)
//            {
//                e.printStackTrace();
//            }
//            return null;
//        }
//
//        //Update the progress
//        @Override
//        protected void onProgressUpdate(Integer... values)
//        {
//            //set the current progress of the progress dialog
//            progressDialog.setProgress(values[0]);
//        }
//
//        //after executing the code in the thread
//        @Override
//        protected void onPostExecute(Void result)
//        {
//            //close the progress dialog
//            progressDialog.dismiss();
//            //initialize the View
//            setContentView(R.layout.activity_home_page);
//        }
//    }

}