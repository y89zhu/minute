package koala.minute;
// cards reference: https://pixabay.com/en/ace-cards-club-diamond-heart-jack-159856/
// background reference: https://pixabay.com/zh/%E6%89%91%E5%85%8B-%E8%B5%8C%E5%9C%BA-%E6%B8%B8%E6%88%8F-%E8%BF%90%E6%B0%94-%E6%92%AD%E6%94%BE-%E7%8E%A9%E8%80%8D-%E6%9C%BA%E4%BC%9A-%E6%88%90%E5%8A%9F-%E6%8B%89%E6%96%AF%E7%BB%B4%E5%8A%A0%E6%96%AF-742756/

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.Random;

import tyrantgit.explosionfield.ExplosionField;

public class Blackjack extends AppCompatActivity {

    Firebase firebaseRef;
    Firebase usersRef;
    String UID;
    private ExplosionField explosion;
    private int dealerDowncardSuit = 0;
    private int dealerDowncardRank = 0;
    private int scoreDealer = 0;
    private int scorePlayer = 0;
    private int dealerCardNum = 1;
    private int playerCardNum = 11;
    private int aceDealer = 0;
    private int acePlayer = 0;
    private ArrayList<Integer> deckDealer = new ArrayList<>();
    private ArrayList<Integer> deckPlayer = new ArrayList<>();

    // animation (David) -----------------
    private Animation animationalpha; // flashing
    private Animation animationscale; // size: regular -> Bigger -> regular
    private Animation animationrotate; // rotate
    private TranslateAnimation tanimation; // moving image
    // http://stackoverflow.com/questions/4689918/move-image-from-left-to-right-in-android
    // init in  oncreate
    // -----------------------------------

    public boolean gameOver = false;
    public boolean doubleDownBoolean = false;
    int base = 10;
    String currentScore;
    ImageView dealer2;
    Button start;
    Button restart;
    ImageView hit;
    ImageView stand;
    ImageView doubleDown;
    TextView messageTextView;
    TextView scoreTextView;

    public void Initialization(View view) {
        // Dealer initialization
        int suit = getRandomCardSuit();
        int rank = getRandomCardRank();
        setCard(suit, rank, dealerCardNum);
        dealerCardNum = 3;
        calculateScore(rank, 1);
        System.out.println("Dealer card: " + rank + " Suit: " + suit);

        suit = getRandomCardSuit();
        rank = getRandomCardRank();
        dealerDowncardSuit = suit;
        dealerDowncardRank = rank;
        calculateScore(rank, 1);
        dealer2.setImageResource(R.drawable.cardback);
        System.out.println("Dealer card: " + rank + " Suit: " + suit + " scoredealer: " + scoreDealer);

        // Player initialization
        for (int i = 0; i < 2; i++) {
            suit = getRandomCardSuit();
            rank = getRandomCardRank();
            setCard(suit, rank, playerCardNum);
            playerCardNum++;
            calculateScore(rank, 2);
        }

        start.setVisibility(View.INVISIBLE);
        hit.setVisibility(View.VISIBLE);
        stand.setVisibility(View.VISIBLE);
        doubleDown.setVisibility(View.VISIBLE);

        // Dealer is blackjack
        if (scoreDealer == 21) {
            stand(view);
        }
    }

    public int getRandomCardRank() {
        Random random = new Random();
        return random.nextInt(13)+1;
    }

    public int getRandomCardSuit() {
        Random random = new Random();
        return random.nextInt(4)+1;
    }

    // Calculate score towards dealer or player and call gameover() when necessary
    public void calculateScore(int rank, int target) {
        if (rank > 10) {
            rank = 10;
        }

        // Record number of ace and add cards other than ace to deck(ArrayList)
        // Source - 1: dealer // 2:player
        if (target == 1) {
            if (rank == 1) {
                aceDealer++;
            } else {
                deckDealer.add(rank);
            }
        } else if (target == 2) {
            if (rank == 1) {
                acePlayer++;
            } else {
                deckPlayer.add(rank);
            }
        }

        int result = 0;
        // Calculate the result
        if (target == 1) {
            for (int i = 0; i < deckDealer.size(); i++) {
                result = result + deckDealer.get(i);
                if (result > 21) {
                    gameover(target);
                    break;
                }
            }

            int aceNumber = aceDealer;
            while (aceNumber != 0) {
                if (21 - result < aceNumber) {
                    gameover(target);
                    break;
                }

                if (21 - result >= 11) {
                    result = result + 11;
                } else {
                    result = result + 1;
                }
                aceNumber--;
            }
        } else if (target == 2) {
            for (int i = 0; i < deckPlayer.size(); i++) {
                result = result + deckPlayer.get(i);
                if (result > 21) {
                    gameover(target);
                    break;
                }
            }

            int aceNumber = acePlayer;
            while (aceNumber != 0) {
                if (21 - result < aceNumber) {
                    gameover(target);
                    break;
                }

                if (21 - result >= 11) {
                    result = result + 11;
                } else {
                    result = result + 1;
                }
                aceNumber--;
            }
        }

        // Set the result
        if (target == 1) {
            scoreDealer = result;
        } else if (target == 2){
            scorePlayer = result;
        }
    }

    public void hit(View view) {
        view.startAnimation(animationalpha);
        int suit = getRandomCardSuit();
        int rank = getRandomCardRank();
        setCard(suit, rank, playerCardNum);
        playerCardNum++;
        calculateScore(rank, 2);
        System.out.println("Score Player: " + scorePlayer);
        //String message = "Max Score:" + Integer.toString(scorePlayer);
        //scorePlayerTextView.setText(message);
    }

    public void stand(View view) {
        hit.setVisibility(View.INVISIBLE);
        stand.setVisibility(View.INVISIBLE);
        setCard(dealerDowncardSuit, dealerDowncardRank, 2);

        while (scoreDealer < 17) {
            int suit = getRandomCardSuit();
            int rank = getRandomCardRank();
            setCard(suit, rank, dealerCardNum);
            dealerCardNum++;
            calculateScore(rank, 1);
            System.out.println("Score Dealer: " + scoreDealer);
        }

        if (!gameOver) {
            if (scoreDealer < scorePlayer) {
                gameover(1);
            } else if (scoreDealer > scorePlayer) {
                gameover(2);
            } else {
                gameover(3);
            }
        }
    }

    public void gameover(int target) {
        gameOver = true;
        setCard(dealerDowncardSuit, dealerDowncardRank, 2);
        messageTextView.setVisibility(View.VISIBLE);
        hit.setVisibility(View.INVISIBLE);
        stand.setVisibility(View.INVISIBLE);
        doubleDown.setVisibility(View.INVISIBLE);

        System.out.println("Gameover, player: " + scorePlayer + " dealer:" + scoreDealer);

        if (doubleDownBoolean) {
            base = base * 2;
        }

        if (target == 1) {
            String message = "Dealer Lost";
            messageTextView.setTextColor(Color.parseColor("#CC00FF00"));
            messageTextView.setText(message);
            messageTextView.startAnimation(animationrotate);
            updateScore(base);
            //System.out.println("Dealer Lost");
            explosion.explode(findViewById(R.id.dealer1));
            explosion.explode(findViewById(R.id.dealer2));
            explosion.explode(findViewById(R.id.dealer3));
            explosion.explode(findViewById(R.id.dealer4));
            explosion.explode(findViewById(R.id.dealer5));
        } else if (target == 2) {
            String message = "Player Lost";
            messageTextView.setTextColor(Color.parseColor("#CCFF0000"));
            messageTextView.setText(message);
            messageTextView.startAnimation(animationrotate);
            updateScore(-base);
            //System.out.println("Player Lost");
            explosion.explode(findViewById(R.id.player1));
            explosion.explode(findViewById(R.id.player2));
            explosion.explode(findViewById(R.id.player3));
            explosion.explode(findViewById(R.id.player4));
            explosion.explode(findViewById(R.id.player5));
        } else {
            String message = "Tie";
            messageTextView.setTextColor(Color.parseColor("#CCFFFFFF"));
            messageTextView.setText(message);
            messageTextView.startAnimation(animationrotate);
            base = 0;
            restart.setVisibility(View.VISIBLE);
            //System.out.println("Tie");
        }
    }

    public void doubleDown(View view) {
        doubleDownBoolean = true;
        hit(view);
        if (!gameOver) {
            stand(view);
        }
    }

    public void restart(View view) {
        finish();
        overridePendingTransition(R.anim.activity_anim,R.anim.activity_finish);
    }

    public void updateScore(int change) {
        String updating = "New score: updating";
        scoreTextView.setText(updating);
        final int changeNew = change;
        usersRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                currentScore = snapshot.getValue(String.class);
                int newScore = Integer.parseInt(currentScore);
                newScore = newScore + changeNew;
                String newScoreString = Integer.toString(newScore);
                usersRef.setValue(newScoreString);
                String message = "New score: " + newScoreString;
                scoreTextView.setText(message);
                restart.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                System.err.println("Blackjack retrieve score error");
            }
        });


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blackjack);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        dealer2 = (ImageView) findViewById(R.id.dealer2);
        start = (Button) findViewById(R.id.start);
        restart = (Button) findViewById(R.id.restart);
        restart.setVisibility(View.INVISIBLE);
        hit = (ImageView) findViewById(R.id.hit);
        hit.setVisibility(View.INVISIBLE);
        stand = (ImageView) findViewById(R.id.stand);
        stand.setVisibility(View.INVISIBLE);
        doubleDown = (ImageView) findViewById(R.id.doubleDown);
        doubleDown.setVisibility(View.INVISIBLE);

        // edit button text color  (David) --------------------
        //start.setTextColor(Color.parseColor("#CCFF0000"));
        //restart.setTextColor(Color.parseColor("#99FF0000"));
        //------------------------------------------------ end

        messageTextView = (TextView) findViewById(R.id.message);
        messageTextView.setVisibility(View.INVISIBLE);
        scoreTextView = (TextView) findViewById(R.id.score);
        //scoreTextView.setVisibility(View.INVISIBLE);

        firebaseRef = new Firebase("https://koalaminute.firebaseio.com/");
        AuthData authData = firebaseRef.getAuth();

        // animation   (David)-----------
        explosion = ExplosionField.attach2Window(this);
        animationalpha = AnimationUtils.loadAnimation(this,R.anim.anim_alpha);
        animationscale = AnimationUtils.loadAnimation(this,R.anim.anim_scale);
        animationrotate = AnimationUtils.loadAnimation(this,R.anim.anim_rotate);
        //-------------------------------end



        if (authData != null) {
            UID = authData.getUid();
            //System.out.println("Blackjack UID: " + UID);
        } else {
            System.out.println("No authData in Blackjack");
        }

        //usersRef = firebaseRef.child("Users");
        /*usersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                System.out.println("There are " + snapshot.getChildrenCount() + " users");
                for (DataSnapshot postSnapshot: snapshot.getChildren()) {
                    User user = postSnapshot.getValue(User.class);
                    System.out.println(user.getdisplayName() + " - " + user.getID());
                }
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {
                System.out.println("The read failed: " + firebaseError.getMessage());
            }
        });*/

        usersRef = firebaseRef.child("Users").child(UID).child("scoreBlackjack");
        usersRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                currentScore = snapshot.getValue(String.class);
                String message = "Current score: " + currentScore;
                scoreTextView.setText(message);
                //System.out.println("Score" + score);
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {
                System.err.println("Blackjack retrieve score error");
            }
        });

    }

    // Set the display of the corresponding card
    public void setCard(int suit, int rank, int target) {
        ImageView imageView;
        // cardID 1-5: Dealer; 11-15:Player
        switch (target) {
            case 1:
                imageView = (ImageView) findViewById(R.id.dealer1); break;
            case 2:
                imageView = (ImageView) findViewById(R.id.dealer2); break;
            case 3:
                imageView = (ImageView) findViewById(R.id.dealer3); break;
            case 4:
                imageView = (ImageView) findViewById(R.id.dealer4); break;
            case 5:
                imageView = (ImageView) findViewById(R.id.dealer5); break;
            case 6:
                imageView = (ImageView) findViewById(R.id.dealer6); break;

            case 11:
                imageView = (ImageView) findViewById(R.id.player1); break;
            case 12:
                imageView = (ImageView) findViewById(R.id.player2); break;
            case 13:
                imageView = (ImageView) findViewById(R.id.player3); break;
            case 14:
                imageView = (ImageView) findViewById(R.id.player4); break;
            case 15:
                imageView = (ImageView) findViewById(R.id.player5); break;
            case 16:
                imageView = (ImageView) findViewById(R.id.player6); break;
            default:
                imageView = (ImageView) findViewById(R.id.dealer1); break;
        }

        switch(suit) {
            case 1:
                switch(rank)
                {
                    case 1:  {imageView.setImageResource(R.drawable.s1); break;}
                    case 2:  {imageView.setImageResource(R.drawable.s2); break;}
                    case 3:  {imageView.setImageResource(R.drawable.s3); break;}
                    case 4:  {imageView.setImageResource(R.drawable.s4); break;}
                    case 5:  {imageView.setImageResource(R.drawable.s5); break;}
                    case 6:  {imageView.setImageResource(R.drawable.s6); break;}
                    case 7:  {imageView.setImageResource(R.drawable.s7); break;}
                    case 8:  {imageView.setImageResource(R.drawable.s8); break;}
                    case 9:  {imageView.setImageResource(R.drawable.s9); break;}
                    case 10: {imageView.setImageResource(R.drawable.s10); break;}
                    case 11: {imageView.setImageResource(R.drawable.s11); break;}
                    case 12: {imageView.setImageResource(R.drawable.s12); break;}
                    case 13: {imageView.setImageResource(R.drawable.s13); break;}
                }
                break;
            case 2:
                switch(rank)
                {
                    case 1:  {imageView.setImageResource(R.drawable.h1); break;}
                    case 2:  {imageView.setImageResource(R.drawable.h2); break;}
                    case 3:  {imageView.setImageResource(R.drawable.h3); break;}
                    case 4:  {imageView.setImageResource(R.drawable.h4); break;}
                    case 5:  {imageView.setImageResource(R.drawable.h5); break;}
                    case 6:  {imageView.setImageResource(R.drawable.h6); break;}
                    case 7:  {imageView.setImageResource(R.drawable.h7); break;}
                    case 8:  {imageView.setImageResource(R.drawable.h8); break;}
                    case 9:  {imageView.setImageResource(R.drawable.h9); break;}
                    case 10: {imageView.setImageResource(R.drawable.h10); break;}
                    case 11: {imageView.setImageResource(R.drawable.h11); break;}
                    case 12: {imageView.setImageResource(R.drawable.h12); break;}
                    case 13: {imageView.setImageResource(R.drawable.h13); break;}
                }
                break;
            case 3:
                switch(rank)
                {
                    case 1:  {imageView.setImageResource(R.drawable.d1); break;}
                    case 2:  {imageView.setImageResource(R.drawable.d2); break;}
                    case 3:  {imageView.setImageResource(R.drawable.d3); break;}
                    case 4:  {imageView.setImageResource(R.drawable.d4); break;}
                    case 5:  {imageView.setImageResource(R.drawable.d5); break;}
                    case 6:  {imageView.setImageResource(R.drawable.d6); break;}
                    case 7:  {imageView.setImageResource(R.drawable.d7); break;}
                    case 8:  {imageView.setImageResource(R.drawable.d8); break;}
                    case 9:  {imageView.setImageResource(R.drawable.d9); break;}
                    case 10: {imageView.setImageResource(R.drawable.d10); break;}
                    case 11: {imageView.setImageResource(R.drawable.d11); break;}
                    case 12: {imageView.setImageResource(R.drawable.d12); break;}
                    case 13: {imageView.setImageResource(R.drawable.d13); break;}
                }
                break;
            case 4:
                switch(rank)
                {
                    case 1:  {imageView.setImageResource(R.drawable.c1); break;}
                    case 2:  {imageView.setImageResource(R.drawable.c2); break;}
                    case 3:  {imageView.setImageResource(R.drawable.c3); break;}
                    case 4:  {imageView.setImageResource(R.drawable.c4); break;}
                    case 5:  {imageView.setImageResource(R.drawable.c5); break;}
                    case 6:  {imageView.setImageResource(R.drawable.c6); break;}
                    case 7:  {imageView.setImageResource(R.drawable.c7); break;}
                    case 8:  {imageView.setImageResource(R.drawable.c8); break;}
                    case 9:  {imageView.setImageResource(R.drawable.c9); break;}
                    case 10: {imageView.setImageResource(R.drawable.c10); break;}
                    case 11: {imageView.setImageResource(R.drawable.c11); break;}
                    case 12: {imageView.setImageResource(R.drawable.c12); break;}
                    case 13: {imageView.setImageResource(R.drawable.c13); break;}
                }
                break;
            default:
                imageView.setImageDrawable(null); break;
        }
        //imageView.startAnimation(animationscale);

        tanimation = new TranslateAnimation(-16.0f,0.0f,-30.0f,0.0f);
        tanimation.setDuration(400);
        //tanimation.setRepeatCount(0);
        //tanimation.setRepeatMode(2);
        imageView.startAnimation(tanimation);

    }
    // add chip for bet? (David)
    public void add_chip(){

    }
    //---------------------------


}
