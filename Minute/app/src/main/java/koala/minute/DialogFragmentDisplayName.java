package koala.minute;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;

public class DialogFragmentDisplayName extends DialogFragment{
    // The activity that creates an instance of this dialog fragment must
    // implement this interface in order to receive event callbacks.
    public interface NoticeDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog);
    }

    // Use this instance of the interface to deliver action events
    NoticeDialogListener mListener;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog_display_name, null);


        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(view)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        EditText displayNameField = (EditText)view.findViewById(R.id.displayName);
                        String displayName = String.valueOf(displayNameField.getText());
                        //System.out.println("INPUT:" + displayName);

                        // Update displayName into database
                        Firebase firebaseRef = new Firebase("https://koalaminute.firebaseio.com/");
                        AuthData authData = firebaseRef.getAuth();
                        String UID;
                        if (authData != null) {
                            UID = authData.getUid();
                            //System.out.println("UID: " + UID);
                            firebaseRef.child("Users").child(UID).child("displayName").setValue(displayName);
                        } else {
                            System.out.println("No authData in displayName dialog");
                        }

                        mListener.onDialogPositiveClick(DialogFragmentDisplayName.this);
                    }
                });

        return builder.create();
    }
}
